using UnrealBuildTool;

public class GGJ2020Target : TargetRules
{
	public GGJ2020Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("GGJ2020");
	}
}
