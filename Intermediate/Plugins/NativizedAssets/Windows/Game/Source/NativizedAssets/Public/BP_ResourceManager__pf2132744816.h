#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/Engine/Classes/Engine/TextRenderActor.h"
#include "BP_ResourceManager__pf2132744816.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/BP_ResourceManager.BP_ResourceManager_C", OverrideNativeName="BP_ResourceManager_C"))
class ABP_ResourceManager_C__pf2132744816 : public ATextRenderActor
{
public:
	GENERATED_BODY()
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Time", Category="Default", MultiLine="true", OverrideNativeName="time"))
	int32 bpv__time__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="People", Category="Default", MultiLine="true", OverrideNativeName="people"))
	int32 bpv__people__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Growth Rate", Category="Default", MultiLine="true", OverrideNativeName="growthRate"))
	float bpv__growthRate__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Ozon Count", Category="Default", MultiLine="true", OverrideNativeName="ozonCount"))
	int32 bpv__ozonCount__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Resourse Count", Category="Default", MultiLine="true", OverrideNativeName="resourseCount"))
	int32 bpv__resourseCount__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Coal Plant", Category="Default", MultiLine="true", OverrideNativeName="coalPlant"))
	int32 bpv__coalPlant__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Solar Plant", Category="Default", MultiLine="true", OverrideNativeName="solarPlant"))
	int32 bpv__solarPlant__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Energy", Category="Default", MultiLine="true", OverrideNativeName="energy"))
	int32 bpv__energy__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Ozone Percent", Category="Default", MultiLine="true", OverrideNativeName="ozonePercent"))
	float bpv__ozonePercent__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Has War", Category="Default", MultiLine="true", OverrideNativeName="hasWar"))
	bool bpv__hasWar__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="Temp_int_Variable"))
	int32 b0l__Temp_int_Variable__pf;
	UPROPERTY(Transient, DuplicateTransient, meta=(OverrideNativeName="K2Node_Event_DeltaSeconds"))
	float b0l__K2Node_Event_DeltaSeconds__pf;
	ABP_ResourceManager_C__pf2132744816(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	void bpf__ExecuteUbergraph_BP_ResourceManager__pf_0(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_ResourceManager__pf_1(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_ResourceManager__pf_2(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_ResourceManager__pf_3(int32 bpp__EntryPoint__pf);
	void bpf__ExecuteUbergraph_BP_ResourceManager__pf_4(int32 bpp__EntryPoint__pf);
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="AddSolarPlant"))
	virtual void bpf__AddSolarPlant__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="AddCoalPlant"))
	virtual void bpf__AddCoalPlant__pf();
	UFUNCTION(BlueprintCallable, meta=(Category, OverrideNativeName="RemoveCoalPlant"))
	virtual void bpf__RemoveCoalPlant__pf();
	UFUNCTION(meta=(Comment="/** Event called every frame, if ticking is enabled */", DisplayName="Tick", ToolTip="Event called every frame, if ticking is enabled", CppFromBpEvent, OverrideNativeName="ReceiveTick"))
	virtual void bpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf);
	UFUNCTION(meta=(Comment="/** Event when play begins for this actor. */", DisplayName="BeginPlay", ToolTip="Event when play begins for this actor.", CppFromBpEvent, OverrideNativeName="ReceiveBeginPlay"))
	virtual void bpf__ReceiveBeginPlay__pf();
public:
};
