#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
class UMaterialInstanceDynamic;
class ABP_Earth_C__pf2132744816;
class USceneComponent;
#include "GM_Main__pf2132744816.generated.h"
UCLASS(config=Game, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/GM_Main.GM_Main_C", OverrideNativeName="GM_Main_C"))
class AGM_Main_C__pf2132744816 : public AGameModeBase
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, NonTransactional, meta=(Category="Default", OverrideNativeName="DefaultSceneRoot"))
	USceneComponent* bpv__DefaultSceneRoot__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Earth", Category="Default", MultiLine="true", OverrideNativeName="Earth"))
	ABP_Earth_C__pf2132744816* bpv__Earth__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Ozone Intensity", Category="Default", MultiLine="true", OverrideNativeName="OzoneIntensity"))
	float bpv__OzoneIntensity__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Ozone Material", Category="Default", MultiLine="true", OverrideNativeName="OzoneMaterial"))
	UMaterialInstanceDynamic* bpv__OzoneMaterial__pf;
	AGM_Main_C__pf2132744816(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
public:
};
