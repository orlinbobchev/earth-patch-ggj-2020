#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
class UTextBlock;
class UImage;
#include "UI_GameOVer__pf3053510930.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/UI/UI_GameOVer.UI_GameOVer_C", OverrideNativeName="UI_GameOVer_C"))
class UUI_GameOVer_C__pf3053510930 : public UUserWidget
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_27", Category="UI_GameOVer", OverrideNativeName="Image_27"))
	UImage* bpv__Image_27__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_40", Category="UI_GameOVer", OverrideNativeName="Image_40"))
	UImage* bpv__Image_40__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_126", OverrideNativeName="TextBlock_126"))
	UTextBlock* bpv__TextBlock_126__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Time", Category="Default", MultiLine="true", OverrideNativeName="Time"))
	int32 bpv__Time__pf;
	UUI_GameOVer_C__pf3053510930(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetText_0"))
	virtual FText  bpf__GetText_0__pf();
public:
	virtual void GetSlotNames(TArray<FName>& SlotNames) const override;
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
	virtual void InitializeNativeClassData() override;
};
