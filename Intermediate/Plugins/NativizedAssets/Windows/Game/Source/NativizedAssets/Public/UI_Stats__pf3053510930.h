#pragma once
#include "Blueprint/BlueprintSupport.h"
#include "Runtime/UMG/Public/UMG.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
class UImage;
class UTextBlock;
#include "UI_Stats__pf3053510930.generated.h"
UCLASS(config=Engine, Blueprintable, BlueprintType, meta=(ReplaceConverted="/Game/Blueprints/UI/UI_Stats.UI_Stats_C", OverrideNativeName="UI_Stats_C"))
class UUI_Stats_C__pf3053510930 : public UUserWidget
{
public:
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Icon", Category="UI_Stats", OverrideNativeName="Icon"))
	UImage* bpv__Icon__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Icon_1", Category="UI_Stats", OverrideNativeName="Icon_1"))
	UImage* bpv__Icon_1__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Icon_2", Category="UI_Stats", OverrideNativeName="Icon_2"))
	UImage* bpv__Icon_2__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Icon_3", Category="UI_Stats", OverrideNativeName="Icon_3"))
	UImage* bpv__Icon_3__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Icon_4", Category="UI_Stats", OverrideNativeName="Icon_4"))
	UImage* bpv__Icon_4__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Icon_5", Category="UI_Stats", OverrideNativeName="Icon_5"))
	UImage* bpv__Icon_5__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image", Category="UI_Stats", OverrideNativeName="Image"))
	UImage* bpv__Image__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_1", Category="UI_Stats", OverrideNativeName="Image_1"))
	UImage* bpv__Image_1__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_2", Category="UI_Stats", OverrideNativeName="Image_2"))
	UImage* bpv__Image_2__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_3", Category="UI_Stats", OverrideNativeName="Image_3"))
	UImage* bpv__Image_3__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_4", Category="UI_Stats", OverrideNativeName="Image_4"))
	UImage* bpv__Image_4__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_5", Category="UI_Stats", OverrideNativeName="Image_5"))
	UImage* bpv__Image_5__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_6", Category="UI_Stats", OverrideNativeName="Image_6"))
	UImage* bpv__Image_6__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_7", Category="UI_Stats", OverrideNativeName="Image_7"))
	UImage* bpv__Image_7__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Image_409", Category="UI_Stats", OverrideNativeName="Image_409"))
	UImage* bpv__Image_409__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_2", OverrideNativeName="TextBlock_2"))
	UTextBlock* bpv__TextBlock_2__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_195", OverrideNativeName="TextBlock_195"))
	UTextBlock* bpv__TextBlock_195__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_250", OverrideNativeName="TextBlock_250"))
	UTextBlock* bpv__TextBlock_250__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_326", OverrideNativeName="TextBlock_326"))
	UTextBlock* bpv__TextBlock_326__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_362", OverrideNativeName="TextBlock_362"))
	UTextBlock* bpv__TextBlock_362__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_401", OverrideNativeName="TextBlock_401"))
	UTextBlock* bpv__TextBlock_401__pf;
	UPROPERTY(Export, meta=(DisplayName="TextBlock_449", OverrideNativeName="TextBlock_449"))
	UTextBlock* bpv__TextBlock_449__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="War", Category="UI_Stats", OverrideNativeName="War"))
	UImage* bpv__War__pf;
	UPROPERTY(BlueprintReadWrite, Export, meta=(DisplayName="Warning", Category="UI_Stats", OverrideNativeName="Warning"))
	UImage* bpv__Warning__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Population", Category="Default", MultiLine="true", OverrideNativeName="Population"))
	int32 bpv__Population__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Ozone", Category="Default", MultiLine="true", OverrideNativeName="Ozone"))
	float bpv__Ozone__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Resources", Category="Default", MultiLine="true", OverrideNativeName="Resources"))
	int32 bpv__Resources__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Coal", Category="Default", MultiLine="true", OverrideNativeName="Coal"))
	int32 bpv__Coal__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Solar", Category="Default", MultiLine="true", OverrideNativeName="Solar"))
	int32 bpv__Solar__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Energy", Category="Default", MultiLine="true", OverrideNativeName="Energy"))
	int32 bpv__Energy__pf;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(DisplayName="Time", Category="Default", MultiLine="true", OverrideNativeName="Time"))
	int32 bpv__Time__pf;
	UUI_Stats_C__pf3053510930(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph) override;
	static void __CustomDynamicClassInitialization(UDynamicClass* InDynamicClass);
	static void __StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	static void __StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad);
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="Get Population"))
	virtual FText  bpf__GetxPopulation__pfT();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="Get Ozone"))
	virtual FText  bpf__GetxOzone__pfT();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetResources"))
	virtual FText  bpf__GetResources__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetCoal"))
	virtual FText  bpf__GetCoal__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetSolar"))
	virtual FText  bpf__GetSolar__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetEnergy"))
	virtual FText  bpf__GetEnergy__pf();
	UFUNCTION(BlueprintCallable, BlueprintPure, meta=(Category, OverrideNativeName="GetText_0"))
	virtual FText  bpf__GetText_0__pf();
public:
	virtual void GetSlotNames(TArray<FName>& SlotNames) const override;
	virtual void PreSave(const class ITargetPlatform* TargetPlatform) override;
	virtual void InitializeNativeClassData() override;
};
