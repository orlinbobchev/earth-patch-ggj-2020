#include "NativizedAssets.h"
#include "UI_Stats__pf3053510930.h"
#include "GeneratedCodeHelpers.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/UMG/Public/Components/CanvasPanel.h"
#include "Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/Engine/Classes/Engine/Font.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/Engine/Classes/Engine/Texture2D.h"
#include "Runtime/UMG/Public/Components/Border.h"
#include "Runtime/UMG/Public/Components/Widget.h"
#include "Runtime/UMG/Public/Components/Visual.h"
#include "Runtime/CoreUObject/Public/UObject/NoExportTypes.h"
#include "Runtime/UMG/Public/Components/PanelSlot.h"
#include "Runtime/UMG/Public/Components/PanelWidget.h"
#include "Runtime/UMG/Public/Components/SlateWrapperTypes.h"
#include "Runtime/UMG/Public/Slate/WidgetTransform.h"
#include "Runtime/SlateCore/Public/Layout/Clipping.h"
#include "Runtime/UMG/Public/Blueprint/WidgetNavigation.h"
#include "Runtime/SlateCore/Public/Input/NavigationReply.h"
#include "Runtime/SlateCore/Public/Types/SlateEnums.h"
#include "Runtime/SlateCore/Public/Layout/FlowDirection.h"
#include "Runtime/UMG/Public/Binding/PropertyBinding.h"
#include "Runtime/UMG/Public/Binding/DynamicPropertyPath.h"
#include "Runtime/PropertyPath/Public/PropertyPathHelpers.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Engine/EngineBaseTypes.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Engine/Classes/Engine/NetSerialization.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/AssetUserData.h"
#include "Runtime/Engine/Public/ComponentInstanceDataCache.h"
#include "Runtime/Engine/Classes/EdGraph/EdGraphPin.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_AssetUserData.h"
#include "Runtime/Engine/Classes/GameFramework/PhysicsVolume.h"
#include "Runtime/Engine/Classes/GameFramework/Volume.h"
#include "Runtime/Engine/Classes/Engine/Brush.h"
#include "Runtime/Engine/Classes/Components/BrushComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "Runtime/Engine/Public/SceneTypes.h"
#include "Runtime/Engine/Classes/VT/RuntimeVirtualTexture.h"
#include "Runtime/Engine/Public/VT/RuntimeVirtualTextureEnum.h"
#include "Runtime/Engine/Classes/VT/RuntimeVirtualTextureStreamingProxy.h"
#include "Runtime/Engine/Classes/Engine/Texture.h"
#include "Runtime/Engine/Classes/Engine/StreamableRenderAsset.h"
#include "Runtime/Engine/Classes/Engine/TextureDefines.h"
#include "Runtime/Engine/Classes/VT/VirtualTextureBuildSettings.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodyInstance.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterial.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsSettingsEnums.h"
#include "Runtime/Engine/Classes/PhysicalMaterials/PhysicalMaterialPropertyBase.h"
#include "Runtime/Engine/Classes/Vehicles/TireType.h"
#include "Runtime/Engine/Classes/Engine/DataAsset.h"
#include "Runtime/InputCore/Classes/InputCoreTypes.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Engine/SubsurfaceProfile.h"
#include "Runtime/Engine/Classes/Materials/Material.h"
#include "Runtime/Engine/Public/MaterialShared.h"
#include "Runtime/Engine/Classes/Materials/MaterialExpression.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunction.h"
#include "Runtime/Engine/Classes/Materials/MaterialFunctionInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollection.h"
#include "Runtime/Engine/Classes/Engine/BlendableInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstance.h"
#include "Runtime/Engine/Classes/Materials/MaterialLayersFunctions.h"
#include "Runtime/Engine/Classes/Engine/FontImportOptions.h"
#include "Runtime/SlateCore/Public/Fonts/CompositeFont.h"
#include "Runtime/SlateCore/Public/Fonts/FontProviderInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceBasePropertyOverrides.h"
#include "Runtime/Engine/Public/StaticParameterSet.h"
#include "Runtime/Engine/Classes/GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerState.h"
#include "Runtime/Engine/Classes/GameFramework/Info.h"
#include "Runtime/Engine/Classes/Components/BillboardComponent.h"
#include "Runtime/Engine/Classes/GameFramework/LocalMessage.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineReplStructs.h"
#include "Runtime/CoreUObject/Public/UObject/CoreOnline.h"
#include "Runtime/Engine/Classes/GameFramework/EngineMessage.h"
#include "Runtime/Engine/Classes/GameFramework/PawnMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/NavMovementComponent.h"
#include "Runtime/Engine/Classes/GameFramework/MovementComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationTypes.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAgentInterface.h"
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Navigation/PathFollowingComponent.h"
#include "Runtime/NavigationSystem/Public/NavigationData.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataInterface.h"
#include "Runtime/AIModule/Classes/AIResourceInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/PathFollowingAgentInterface.h"
#include "Runtime/AIModule/Classes/BrainComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/Blackboard/BlackboardKeyType.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "Runtime/AIModule/Classes/Perception/AISenseConfig.h"
#include "Runtime/AIModule/Classes/Perception/AISense.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionTypes.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionSystem.h"
#include "Runtime/AIModule/Classes/AISubsystem.h"
#include "Runtime/AIModule/Classes/AISystem.h"
#include "Runtime/Engine/Classes/AI/AISystemBase.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeManager.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTCompositeNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTNode.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskOwnerInterface.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTTaskNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTService.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTAuxiliaryNode.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BTDecorator.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeTypes.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTreeComponent.h"
#include "Runtime/GameplayTags/Classes/GameplayTagContainer.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryManager.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQuery.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryOption.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryGenerator.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryNode.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/Items/EnvQueryItemType.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryTest.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryTypes.h"
#include "Runtime/AIModule/Classes/DataProviders/AIDataProvider.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryContext.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EnvQueryInstanceBlueprintWrapper.h"
#include "Runtime/AIModule/Classes/EnvironmentQuery/EQSQueryResultSourceInterface.h"
#include "Runtime/AIModule/Classes/Blueprint/AIAsyncTaskBlueprintProxy.h"
#include "Runtime/AIModule/Classes/AITypes.h"
#include "Runtime/AIModule/Classes/HotSpots/AIHotSpotManager.h"
#include "Runtime/AIModule/Classes/Navigation/NavLocalGridManager.h"
#include "Runtime/AIModule/Classes/Perception/AISenseEvent.h"
#include "Runtime/AIModule/Classes/Actions/PawnActionsComponent.h"
#include "Runtime/AIModule/Classes/Actions/PawnAction.h"
#include "Runtime/GameplayTasks/Classes/GameplayTasksComponent.h"
#include "Runtime/GameplayTasks/Classes/GameplayTask.h"
#include "Runtime/GameplayTasks/Classes/GameplayTaskResource.h"
#include "Runtime/NavigationSystem/Public/NavFilters/NavigationQueryFilter.h"
#include "Runtime/NavigationSystem/Public/NavAreas/NavArea.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavAreaBase.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionListenerInterface.h"
#include "Runtime/AIModule/Classes/GenericTeamAgentInterface.h"
#include "Runtime/Engine/Public/VisualLogger/VisualLoggerDebugSnapshotInterface.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavRelevantInterface.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetup.h"
#include "Runtime/Engine/Classes/PhysicsEngine/AggregateGeom.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphereElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ShapeElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BoxElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/SphylElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConvexElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/TaperedCapsuleElem.h"
#include "Runtime/Engine/Classes/PhysicsEngine/BodySetupEnums.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerInput.h"
#include "Runtime/Engine/Classes/Matinee/MatineeActor.h"
#include "Runtime/Engine/Classes/Matinee/InterpData.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroup.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrack.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInst.h"
#include "Runtime/Engine/Classes/Engine/InterpCurveEdSetup.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupDirector.h"
#include "Runtime/Engine/Classes/Matinee/InterpGroupInst.h"
#include "Runtime/Engine/Classes/Components/ChildActorComponent.h"
#include "Runtime/Engine/Classes/GameFramework/DamageType.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SkinnedMeshComponent.h"
#include "Runtime/Engine/Classes/Components/MeshComponent.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMesh.h"
#include "Runtime/Engine/Classes/Animation/Skeleton.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSocket.h"
#include "Runtime/Engine/Classes/Animation/SmartName.h"
#include "Runtime/Engine/Classes/Animation/BlendProfile.h"
#include "Runtime/Engine/Public/BoneContainer.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_PreviewMeshProvider.h"
#include "Runtime/Engine/Public/Components.h"
#include "Runtime/Engine/Public/PerPlatformProperties.h"
#include "Runtime/Engine/Public/SkeletalMeshReductionSettings.h"
#include "Runtime/Engine/Classes/Animation/AnimSequence.h"
#include "Runtime/Engine/Classes/Animation/AnimSequenceBase.h"
#include "Runtime/Engine/Classes/Animation/AnimationAsset.h"
#include "Runtime/Engine/Classes/Animation/AnimMetaData.h"
#include "Runtime/Engine/Public/Animation/AnimTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimLinkableElement.h"
#include "Runtime/Engine/Classes/Animation/AnimMontage.h"
#include "Runtime/Engine/Classes/Animation/AnimCompositeBase.h"
#include "Runtime/Engine/Public/AlphaBlend.h"
#include "Runtime/Engine/Classes/Curves/CurveFloat.h"
#include "Runtime/Engine/Classes/Curves/CurveBase.h"
#include "Runtime/Engine/Classes/Curves/RichCurve.h"
#include "Runtime/Engine/Classes/Curves/RealCurve.h"
#include "Runtime/Engine/Classes/Curves/IndexedCurve.h"
#include "Runtime/Engine/Classes/Curves/KeyHandle.h"
#include "Runtime/Engine/Classes/Animation/AnimEnums.h"
#include "Runtime/Engine/Classes/Animation/TimeStretchCurve.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotify.h"
#include "Runtime/Engine/Classes/Animation/AnimNotifies/AnimNotifyState.h"
#include "Runtime/Engine/Public/Animation/AnimCurveTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimCurveCompressionSettings.h"
#include "Runtime/Engine/Classes/Animation/AnimCurveCompressionCodec.h"
#include "Runtime/Engine/Classes/Animation/AnimCurveCompressionCodec_CompressedRichCurve.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsAsset.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicalAnimationComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintTemplate.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintInstance.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintTypes.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintDrives.h"
#include "Runtime/Engine/Classes/EditorFramework/ThumbnailInfo.h"
#include "Runtime/Engine/Public/Animation/NodeMappingContainer.h"
#include "Runtime/Engine/Public/Animation/NodeMappingProviderInterface.h"
#include "Runtime/Engine/Classes/Animation/MorphTarget.h"
#include "Runtime/Engine/Classes/Animation/AnimInstance.h"
#include "Runtime/Engine/Public/Animation/AnimNotifyQueue.h"
#include "Runtime/Engine/Public/Animation/PoseSnapshot.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingAssetBase.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshSampling.h"
#include "Runtime/Engine/Public/Animation/SkinWeightProfile.h"
#include "Runtime/Engine/Classes/Engine/SkeletalMeshLODSettings.h"
#include "Runtime/Engine/Classes/Engine/Blueprint.h"
#include "Runtime/Engine/Classes/Engine/BlueprintCore.h"
#include "Runtime/Engine/Classes/Engine/SimpleConstructionScript.h"
#include "Runtime/Engine/Classes/Engine/SCS_Node.h"
#include "Runtime/Engine/Classes/Engine/BlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/TimelineTemplate.h"
#include "Runtime/Engine/Classes/Components/TimelineComponent.h"
#include "Runtime/Engine/Classes/Curves/CurveVector.h"
#include "Runtime/Engine/Classes/Curves/CurveLinearColor.h"
#include "Runtime/Engine/Classes/Engine/InheritableComponentHandler.h"
#include "Runtime/Engine/Classes/Interfaces/Interface_CollisionDataProvider.h"
#include "Runtime/Engine/Classes/Animation/AnimBlueprintGeneratedClass.h"
#include "Runtime/Engine/Classes/Engine/DynamicBlueprintBinding.h"
#include "Runtime/Engine/Classes/Animation/AnimStateMachineTypes.h"
#include "Runtime/Engine/Classes/Animation/AnimClassInterface.h"
#include "Runtime/Engine/Classes/Animation/AnimNodeBase.h"
#include "Runtime/Engine/Public/SingleAnimationPlayData.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationFactory.h"
#include "Runtime/ClothingSystemRuntimeInterface/Public/ClothingSimulationInteractor.h"
#include "Runtime/Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationAvoidanceTypes.h"
#include "Runtime/Engine/Classes/GameFramework/RootMotionSource.h"
#include "Runtime/Engine/Public/AI/RVOAvoidanceInterface.h"
#include "Runtime/Engine/Classes/Interfaces/NetworkPredictionInterface.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/ShapeComponent.h"
#include "Runtime/Engine/Classes/Engine/Player.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstDirector.h"
#include "Runtime/Engine/Classes/GameFramework/HUD.h"
#include "Runtime/Engine/Classes/Engine/Canvas.h"
#include "Runtime/Engine/Classes/Debug/ReporterGraph.h"
#include "Runtime/Engine/Classes/Debug/ReporterBase.h"
#include "Runtime/Engine/Classes/GameFramework/DebugTextInfo.h"
#include "Runtime/Engine/Classes/Camera/PlayerCameraManager.h"
#include "Runtime/Engine/Classes/Camera/CameraTypes.h"
#include "Runtime/Engine/Classes/Engine/Scene.h"
#include "Runtime/Engine/Classes/Engine/TextureCube.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier.h"
#include "Runtime/Engine/Classes/Particles/EmitterCameraLensEffectBase.h"
#include "Runtime/Engine/Classes/Particles/Emitter.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Particles/ParticleEmitter.h"
#include "Runtime/Engine/Public/ParticleHelper.h"
#include "Runtime/Engine/Classes/Particles/ParticleLODLevel.h"
#include "Runtime/Engine/Classes/Particles/ParticleModuleRequired.h"
#include "Runtime/Engine/Classes/Particles/ParticleModule.h"
#include "Runtime/Engine/Classes/Particles/ParticleSpriteEmitter.h"
#include "Runtime/Engine/Classes/Distributions/DistributionFloat.h"
#include "Runtime/Engine/Classes/Distributions/Distribution.h"
#include "Runtime/Engine/Classes/Particles/SubUVAnimation.h"
#include "Runtime/Engine/Classes/Particles/TypeData/ParticleModuleTypeDataBase.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawn.h"
#include "Runtime/Engine/Classes/Particles/Spawn/ParticleModuleSpawnBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventGenerator.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventBase.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventSendToGame.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbit.h"
#include "Runtime/Engine/Classes/Particles/Orbit/ParticleModuleOrbitBase.h"
#include "Runtime/Engine/Classes/Distributions/DistributionVector.h"
#include "Runtime/Engine/Classes/Particles/Event/ParticleModuleEventReceiverBase.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemReplay.h"
#include "Runtime/Engine/Classes/Camera/CameraModifier_CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraShake.h"
#include "Runtime/Engine/Classes/Camera/CameraAnim.h"
#include "Runtime/Engine/Classes/Camera/CameraAnimInst.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackMove.h"
#include "Runtime/Engine/Classes/Matinee/InterpTrackInstMove.h"
#include "Runtime/Engine/Classes/Camera/CameraActor.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/GameFramework/CheatManager.h"
#include "Runtime/Engine/Classes/Engine/DebugCameraController.h"
#include "Runtime/Engine/Classes/Components/DrawFrustumComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ForceFeedbackEffect.h"
#include "Runtime/Engine/Classes/Engine/NetConnection.h"
#include "Runtime/Engine/Classes/Engine/ChildConnection.h"
#include "Runtime/Engine/Classes/Engine/PackageMapClient.h"
#include "Runtime/Engine/Classes/Engine/NetDriver.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Engine/Level.h"
#include "Runtime/Engine/Classes/Components/ModelComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelActorContainer.h"
#include "Runtime/Engine/Classes/Engine/LevelScriptActor.h"
#include "Runtime/Engine/Classes/Engine/NavigationObjectBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavigationDataChunk.h"
#include "Runtime/Engine/Classes/Engine/MapBuildDataRegistry.h"
#include "Runtime/Engine/Classes/GameFramework/WorldSettings.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemConfig.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPhysicsVolume.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsCollisionHandler.h"
#include "Runtime/Engine/Classes/Sound/SoundBase.h"
#include "Runtime/Engine/Classes/Sound/SoundClass.h"
#include "Runtime/Engine/Classes/Sound/SoundMix.h"
#include "Runtime/Engine/Public/IAudioExtensionPlugin.h"
#include "Runtime/Engine/Classes/Sound/SoundConcurrency.h"
#include "Runtime/Engine/Classes/Sound/SoundAttenuation.h"
#include "Runtime/Engine/Classes/Engine/Attenuation.h"
#include "Runtime/Engine/Classes/Sound/SoundSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSubmix.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectPreset.h"
#include "Runtime/Engine/Public/IAmbisonicsMixer.h"
#include "Runtime/Engine/Classes/Sound/SoundWave.h"
#include "Runtime/AudioPlatformConfiguration/Public/AudioCompressionSettings.h"
#include "Runtime/Engine/Classes/Sound/SoundGroups.h"
#include "Runtime/Engine/Classes/Engine/CurveTable.h"
#include "Runtime/Engine/Classes/Sound/SoundSubmixSend.h"
#include "Runtime/Engine/Classes/Sound/SoundEffectSource.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBusSend.h"
#include "Runtime/Engine/Classes/Sound/SoundSourceBus.h"
#include "Runtime/Engine/Classes/GameFramework/GameModeBase.h"
#include "Runtime/Engine/Classes/GameFramework/GameSession.h"
#include "Runtime/Engine/Classes/GameFramework/GameStateBase.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawn.h"
#include "Runtime/Engine/Classes/GameFramework/DefaultPawn.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/Engine/StaticMeshSocket.h"
#include "Runtime/Engine/Classes/AI/Navigation/NavCollisionBase.h"
#include "Runtime/StaticMeshDescription/Public/StaticMeshDescription.h"
#include "Runtime/MeshDescription/Public/MeshDescriptionBase.h"
#include "Runtime/MeshDescription/Public/MeshTypes.h"
#include "Runtime/Engine/Classes/Engine/TextureStreamingTypes.h"
#include "Runtime/Engine/Classes/GameFramework/FloatingPawnMovement.h"
#include "Runtime/Engine/Classes/GameFramework/SpectatorPawnMovement.h"
#include "Runtime/Engine/Classes/Engine/ServerStatReplicator.h"
#include "Runtime/Engine/Classes/GameFramework/GameNetworkManager.h"
#include "Runtime/Engine/Classes/Sound/AudioVolume.h"
#include "Runtime/Engine/Classes/Sound/ReverbEffect.h"
#include "Runtime/Engine/Classes/Engine/BookmarkBase.h"
#include "Runtime/Engine/Classes/Engine/BookMark.h"
#include "Runtime/Engine/Classes/Components/LineBatchComponent.h"
#include "Runtime/Engine/Classes/Engine/LevelStreaming.h"
#include "Runtime/Engine/Classes/Engine/LevelStreamingVolume.h"
#include "Runtime/Engine/Classes/Engine/DemoNetDriver.h"
#include "Runtime/Engine/Classes/Particles/ParticleEventManager.h"
#include "Runtime/Engine/Classes/AI/NavigationSystemBase.h"
#include "Runtime/Engine/Classes/AI/Navigation/AvoidanceManager.h"
#include "Runtime/Engine/Classes/Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "Runtime/Engine/Classes/Engine/GameViewportClient.h"
#include "Runtime/Engine/Classes/Engine/ScriptViewportClient.h"
#include "Runtime/Engine/Classes/Engine/Console.h"
#include "Runtime/Engine/Classes/Engine/DebugDisplayProperty.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"
#include "Runtime/Engine/Classes/GameFramework/GameUserSettings.h"
#include "Runtime/Engine/Classes/Engine/AssetManager.h"
#include "Runtime/Engine/Classes/Engine/EngineCustomTimeStep.h"
#include "Runtime/Engine/Classes/Engine/TimecodeProvider.h"
#include "Runtime/Engine/Classes/GameFramework/OnlineSession.h"
#include "Runtime/Engine/Classes/Materials/MaterialParameterCollectionInstance.h"
#include "Runtime/Engine/Classes/Engine/WorldComposition.h"
#include "Runtime/Engine/Classes/Particles/WorldPSCPool.h"
#include "Runtime/Engine/Classes/Engine/Channel.h"
#include "Runtime/Engine/Classes/Engine/ReplicationDriver.h"
#include "Runtime/Engine/Classes/GameFramework/TouchInterface.h"
#include "Runtime/Engine/Classes/Haptics/HapticFeedbackEffect_Base.h"
#include "Runtime/Engine/Classes/Engine/LatentActionManager.h"
#include "Runtime/SlateCore/Public/Layout/Geometry.h"
#include "Runtime/SlateCore/Public/Input/Events.h"
#include "Runtime/SlateCore/Public/Styling/SlateColor.h"
#include "Runtime/SlateCore/Public/Styling/SlateBrush.h"
#include "Runtime/SlateCore/Public/Layout/Margin.h"
#include "Runtime/SlateCore/Public/Styling/SlateTypes.h"
#include "Runtime/UMG/Public/Animation/UMGSequencePlayer.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimation.h"
#include "Runtime/MovieScene/Public/MovieSceneSequence.h"
#include "Runtime/MovieScene/Public/MovieSceneSignedObject.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackIdentifier.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSegment.h"
#include "Runtime/MovieScene/Public/MovieSceneTrack.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvalTemplate.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneTrackImplementation.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationField.h"
#include "Runtime/MovieScene/Public/MovieSceneFrameMigration.h"
#include "Runtime/MovieScene/Public/MovieSceneSequenceID.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneEvaluationKey.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceHierarchy.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceTransform.h"
#include "Runtime/MovieScene/Public/Evaluation/MovieSceneSequenceInstanceData.h"
#include "Runtime/MovieScene/Public/MovieSceneSection.h"
#include "Runtime/MovieScene/Public/MovieSceneObjectBindingID.h"
#include "Runtime/MovieScene/Public/MovieScene.h"
#include "Runtime/MovieScene/Public/MovieSceneSpawnable.h"
#include "Runtime/MovieScene/Public/MovieScenePossessable.h"
#include "Runtime/MovieScene/Public/MovieSceneBinding.h"
#include "Runtime/MovieScene/Public/MovieSceneFwd.h"
#include "Runtime/UMG/Public/Animation/WidgetAnimationBinding.h"
#include "Runtime/Slate/Public/Widgets/Layout/Anchors.h"
#include "Runtime/UMG/Public/Blueprint/DragDropOperation.h"
#include "Runtime/UMG/Public/Components/NamedSlotInterface.h"
#include "Runtime/Engine/Classes/Kismet/KismetTextLibrary.h"
#include "Runtime/Engine/Classes/Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Classes/Engine/Texture2DDynamic.h"
#include "Runtime/Engine/Public/Slate/SlateTextureAtlasInterface.h"
#include "Runtime/Engine/Classes/Slate/SlateBrushAsset.h"
#include "Runtime/UMG/Public/Components/TextWidgetTypes.h"
#include "Runtime/SlateCore/Public/Fonts/FontCache.h"
#include "Runtime/Slate/Public/Framework/Text/TextLayout.h"
#include "Runtime/SlateCore/Public/Fonts/SlateFontInfo.h"


#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
PRAGMA_DISABLE_OPTIMIZATION
UUI_Stats_C__pf3053510930::UUI_Stats_C__pf3053510930(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
	bpv__Icon__pf = nullptr;
	bpv__Icon_1__pf = nullptr;
	bpv__Icon_2__pf = nullptr;
	bpv__Icon_3__pf = nullptr;
	bpv__Icon_4__pf = nullptr;
	bpv__Icon_5__pf = nullptr;
	bpv__Image__pf = nullptr;
	bpv__Image_1__pf = nullptr;
	bpv__Image_2__pf = nullptr;
	bpv__Image_3__pf = nullptr;
	bpv__Image_4__pf = nullptr;
	bpv__Image_5__pf = nullptr;
	bpv__Image_6__pf = nullptr;
	bpv__Image_7__pf = nullptr;
	bpv__Image_409__pf = nullptr;
	bpv__TextBlock_2__pf = nullptr;
	bpv__TextBlock_195__pf = nullptr;
	bpv__TextBlock_250__pf = nullptr;
	bpv__TextBlock_326__pf = nullptr;
	bpv__TextBlock_362__pf = nullptr;
	bpv__TextBlock_401__pf = nullptr;
	bpv__TextBlock_449__pf = nullptr;
	bpv__War__pf = nullptr;
	bpv__Warning__pf = nullptr;
	bpv__Population__pf = 0;
	bpv__Ozone__pf = 0.000000f;
	bpv__Resources__pf = 0;
	bpv__Coal__pf = 0;
	bpv__Solar__pf = 0;
	bpv__Energy__pf = 0;
	bpv__Time__pf = 0;
	bHasScriptImplementedTick = false;
	bHasScriptImplementedPaint = false;
}
PRAGMA_ENABLE_OPTIMIZATION
void UUI_Stats_C__pf3053510930::PostLoadSubobjects(FObjectInstancingGraph* OuterInstanceGraph)
{
	Super::PostLoadSubobjects(OuterInstanceGraph);
}
PRAGMA_DISABLE_OPTIMIZATION
void UUI_Stats_C__pf3053510930::__CustomDynamicClassInitialization(UDynamicClass* InDynamicClass)
{
	ensure(0 == InDynamicClass->ReferencedConvertedFields.Num());
	ensure(0 == InDynamicClass->MiscConvertedSubobjects.Num());
	ensure(0 == InDynamicClass->DynamicBindingObjects.Num());
	ensure(0 == InDynamicClass->ComponentTemplates.Num());
	ensure(0 == InDynamicClass->Timelines.Num());
	ensure(0 == InDynamicClass->ComponentClassOverrides.Num());
	ensure(nullptr == InDynamicClass->AnimClassImplementation);
	InDynamicClass->AssembleReferenceTokenStream();
	FConvertedBlueprintsDependencies::FillUsedAssetsInDynamicClass(InDynamicClass, &__StaticDependencies_DirectlyUsedAssets);
	auto __Local__0 = NewObject<UWidgetTree>(InDynamicClass, UWidgetTree::StaticClass(), TEXT("WidgetTree"), (EObjectFlags)0x00000009);
	InDynamicClass->MiscConvertedSubobjects.Add(__Local__0);
	auto __Local__1 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("CanvasPanel_0"), (EObjectFlags)0x00280008);
	auto& __Local__2 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__1), UPanelWidget::__PPO__Slots() )));
	__Local__2 = TArray<UPanelSlot*> ();
	__Local__2.Reserve(7);
	auto __Local__3 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_18"), (EObjectFlags)0x00280008);
	__Local__3->LayoutData.Offsets.Left = 40.000000f;
	__Local__3->LayoutData.Offsets.Top = 320.000000f;
	__Local__3->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__3->Parent = __Local__1;
	auto __Local__4 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("CanvasPanel_71"), (EObjectFlags)0x00280008);
	auto& __Local__5 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__4), UPanelWidget::__PPO__Slots() )));
	__Local__5 = TArray<UPanelSlot*> ();
	__Local__5.Reserve(12);
	auto __Local__6 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_14"), (EObjectFlags)0x00280008);
	__Local__6->LayoutData.Offsets.Left = 60.000000f;
	__Local__6->LayoutData.Offsets.Top = 40.000000f;
	__Local__6->LayoutData.Offsets.Right = 235.528976f;
	__Local__6->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__6->bAutoSize = true;
	__Local__6->Parent = __Local__4;
	auto __Local__7 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_250"), (EObjectFlags)0x00280008);
	__Local__7->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"36AD08554C6E3378883F59AA0772512C\", \"Ozone Integrity:\")")	);
	__Local__7->Slot = __Local__6;
	__Local__6->Content = __Local__7;
	__Local__5.Add(__Local__6);
	auto __Local__8 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"), (EObjectFlags)0x00280008);
	__Local__8->LayoutData.Offsets.Left = 60.000000f;
	__Local__8->LayoutData.Offsets.Top = 100.000000f;
	__Local__8->LayoutData.Offsets.Right = 151.000000f;
	__Local__8->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__8->bAutoSize = true;
	__Local__8->Parent = __Local__4;
	auto __Local__9 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_195"), (EObjectFlags)0x00280008);
	__Local__9->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"24225BC4403ABF064C823791F197F953\", \"Population:\")")	);
	__Local__9->Slot = __Local__8;
	__Local__8->Content = __Local__9;
	__Local__5.Add(__Local__8);
	auto __Local__10 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_2"), (EObjectFlags)0x00280008);
	__Local__10->LayoutData.Offsets.Left = 60.000000f;
	__Local__10->LayoutData.Offsets.Top = 160.000000f;
	__Local__10->LayoutData.Offsets.Right = 151.000000f;
	__Local__10->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__10->bAutoSize = true;
	__Local__10->Parent = __Local__4;
	auto __Local__11 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_326"), (EObjectFlags)0x00280008);
	__Local__11->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"FF7E9E4146AE2D4C1D51DAA30355971A\", \"Resource:\")")	);
	__Local__11->Slot = __Local__10;
	__Local__10->Content = __Local__11;
	__Local__5.Add(__Local__10);
	auto __Local__12 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_15"), (EObjectFlags)0x00280008);
	__Local__12->LayoutData.Offsets.Left = 60.000000f;
	__Local__12->LayoutData.Offsets.Top = 340.000000f;
	__Local__12->LayoutData.Offsets.Right = 151.000000f;
	__Local__12->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__12->bAutoSize = true;
	__Local__12->Parent = __Local__4;
	auto __Local__13 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_449"), (EObjectFlags)0x00280008);
	__Local__13->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"8A180A0C48188D6C12E1F6948A8EC32E\", \"Energy:\")")	);
	__Local__13->Slot = __Local__12;
	__Local__12->Content = __Local__13;
	__Local__5.Add(__Local__12);
	auto __Local__14 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_3"), (EObjectFlags)0x00280008);
	__Local__14->LayoutData.Offsets.Left = 60.000000f;
	__Local__14->LayoutData.Offsets.Top = 220.000000f;
	__Local__14->LayoutData.Offsets.Right = 151.000000f;
	__Local__14->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__14->bAutoSize = true;
	__Local__14->Parent = __Local__4;
	auto __Local__15 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_362"), (EObjectFlags)0x00280008);
	__Local__15->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"B7492ACF4841046560138B8B840471A8\", \"Coal Plants:\")")	);
	__Local__15->Slot = __Local__14;
	__Local__14->Content = __Local__15;
	__Local__5.Add(__Local__14);
	auto __Local__16 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_4"), (EObjectFlags)0x00280008);
	__Local__16->LayoutData.Offsets.Left = 60.000000f;
	__Local__16->LayoutData.Offsets.Top = 280.000000f;
	__Local__16->LayoutData.Offsets.Right = 151.000000f;
	__Local__16->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__16->bAutoSize = true;
	__Local__16->Parent = __Local__4;
	auto __Local__17 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_401"), (EObjectFlags)0x00280008);
	__Local__17->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"44D5A3D045407A0A72760E8A4D5C4FB4\", \"Solar Plants:\")")	);
	__Local__17->Slot = __Local__16;
	__Local__16->Content = __Local__17;
	__Local__5.Add(__Local__16);
	auto __Local__18 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_16"), (EObjectFlags)0x00280008);
	__Local__18->LayoutData.Offsets.Top = 95.000000f;
	__Local__18->LayoutData.Offsets.Right = 50.000000f;
	__Local__18->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__18->Parent = __Local__4;
	auto __Local__19 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Icon"), (EObjectFlags)0x00280008);
	__Local__19->Brush.ImageSize = FVector2D(253.000000, 252.000000);
	auto& __Local__20 = (*(AccessPrivateProperty<UObject* >(&(__Local__19->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__20 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[0], ECastCheckedType::NullAllowed);
	__Local__19->Slot = __Local__18;
	__Local__18->Content = __Local__19;
	__Local__5.Add(__Local__18);
	auto __Local__21 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_7"), (EObjectFlags)0x00280008);
	__Local__21->LayoutData.Offsets.Top = 215.000000f;
	__Local__21->LayoutData.Offsets.Right = 50.000000f;
	__Local__21->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__21->Parent = __Local__4;
	auto __Local__22 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Icon_1"), (EObjectFlags)0x00280008);
	__Local__22->Brush.ImageSize = FVector2D(600.000000, 600.000000);
	auto& __Local__23 = (*(AccessPrivateProperty<UObject* >(&(__Local__22->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__23 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed);
	__Local__22->Slot = __Local__21;
	__Local__21->Content = __Local__22;
	__Local__5.Add(__Local__21);
	auto __Local__24 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_8"), (EObjectFlags)0x00280008);
	__Local__24->LayoutData.Offsets.Top = 155.000000f;
	__Local__24->LayoutData.Offsets.Right = 50.000000f;
	__Local__24->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__24->Parent = __Local__4;
	auto __Local__25 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Icon_2"), (EObjectFlags)0x00280008);
	__Local__25->Brush.ImageSize = FVector2D(1018.000000, 1024.000000);
	auto& __Local__26 = (*(AccessPrivateProperty<UObject* >(&(__Local__25->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__26 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
	__Local__25->Slot = __Local__24;
	__Local__24->Content = __Local__25;
	__Local__5.Add(__Local__24);
	auto __Local__27 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_17"), (EObjectFlags)0x00280008);
	__Local__27->LayoutData.Offsets.Top = 275.000000f;
	__Local__27->LayoutData.Offsets.Right = 50.000000f;
	__Local__27->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__27->Parent = __Local__4;
	auto __Local__28 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Icon_3"), (EObjectFlags)0x00280008);
	__Local__28->Brush.ImageSize = FVector2D(3118.000000, 3000.000000);
	auto& __Local__29 = (*(AccessPrivateProperty<UObject* >(&(__Local__28->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__29 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[3], ECastCheckedType::NullAllowed);
	__Local__28->Slot = __Local__27;
	__Local__27->Content = __Local__28;
	__Local__5.Add(__Local__27);
	auto __Local__30 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_18"), (EObjectFlags)0x00280008);
	__Local__30->LayoutData.Offsets.Top = 35.000000f;
	__Local__30->LayoutData.Offsets.Right = 50.000000f;
	__Local__30->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__30->Parent = __Local__4;
	auto __Local__31 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Icon_4"), (EObjectFlags)0x00280008);
	__Local__31->Brush.ImageSize = FVector2D(408.000000, 398.000000);
	auto& __Local__32 = (*(AccessPrivateProperty<UObject* >(&(__Local__31->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__32 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[4], ECastCheckedType::NullAllowed);
	__Local__31->Slot = __Local__30;
	__Local__30->Content = __Local__31;
	__Local__5.Add(__Local__30);
	auto __Local__33 = NewObject<UCanvasPanelSlot>(__Local__4, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_19"), (EObjectFlags)0x00280008);
	__Local__33->LayoutData.Offsets.Top = 335.000000f;
	__Local__33->LayoutData.Offsets.Right = 50.000000f;
	__Local__33->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__33->Parent = __Local__4;
	auto __Local__34 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Icon_5"), (EObjectFlags)0x00280008);
	__Local__34->Brush.ImageSize = FVector2D(2456.000000, 2663.000000);
	auto& __Local__35 = (*(AccessPrivateProperty<UObject* >(&(__Local__34->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__35 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[5], ECastCheckedType::NullAllowed);
	__Local__34->Slot = __Local__33;
	__Local__33->Content = __Local__34;
	__Local__5.Add(__Local__33);
	__Local__4->Slot = __Local__3;
	__Local__3->Content = __Local__4;
	__Local__2.Add(__Local__3);
	auto __Local__36 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_3"), (EObjectFlags)0x00280008);
	__Local__36->LayoutData.Offsets.Top = 170.000000f;
	__Local__36->LayoutData.Offsets.Right = 140.000000f;
	__Local__36->LayoutData.Offsets.Bottom = 120.000000f;
	__Local__36->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.000000);
	__Local__36->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.000000);
	__Local__36->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__36->Parent = __Local__1;
	auto __Local__37 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Warning"), (EObjectFlags)0x00280008);
	__Local__37->Brush.ImageSize = FVector2D(850.000000, 746.000000);
	auto& __Local__38 = (*(AccessPrivateProperty<UObject* >(&(__Local__37->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__38 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[6], ECastCheckedType::NullAllowed);
	__Local__37->Slot = __Local__36;
	__Local__37->Visibility = ESlateVisibility::Hidden;
	__Local__36->Content = __Local__37;
	__Local__2.Add(__Local__36);
	auto __Local__39 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_10"), (EObjectFlags)0x00280008);
	__Local__39->LayoutData.Offsets.Left = -150.000000f;
	__Local__39->LayoutData.Offsets.Top = -150.000000f;
	__Local__39->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__39->LayoutData.Anchors.Minimum = FVector2D(0.500000, 1.000000);
	__Local__39->LayoutData.Anchors.Maximum = FVector2D(0.500000, 1.000000);
	__Local__39->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__39->Parent = __Local__1;
	auto __Local__40 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("Button1"), (EObjectFlags)0x00280008);
	auto& __Local__41 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__40), UPanelWidget::__PPO__Slots() )));
	__Local__41 = TArray<UPanelSlot*> ();
	__Local__41.Reserve(6);
	auto __Local__42 = NewObject<UCanvasPanelSlot>(__Local__40, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_2"), (EObjectFlags)0x00280008);
	__Local__42->LayoutData.Offsets.Top = -5.000000f;
	__Local__42->LayoutData.Offsets.Bottom = 60.000000f;
	__Local__42->Parent = __Local__40;
	auto __Local__43 = NewObject<UBorder>(__Local__0, UBorder::StaticClass(), TEXT("Border_232"), (EObjectFlags)0x00280008);
	__Local__43->ContentColorAndOpacity = FLinearColor(0.000000, 0.000000, 0.000000, 1.000000);
	__Local__43->BrushColor = FLinearColor(0.000000, 0.000000, 0.000000, 1.000000);
	__Local__43->Slot = __Local__42;
	__Local__42->Content = __Local__43;
	__Local__41.Add(__Local__42);
	auto __Local__44 = NewObject<UCanvasPanelSlot>(__Local__40, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"), (EObjectFlags)0x00280008);
	__Local__44->LayoutData.Offsets.Left = 10.000000f;
	__Local__44->LayoutData.Offsets.Right = 50.000000f;
	__Local__44->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__44->Parent = __Local__40;
	auto __Local__45 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_409"), (EObjectFlags)0x00280008);
	__Local__45->Brush.ImageSize = FVector2D(3118.000000, 3000.000000);
	auto& __Local__46 = (*(AccessPrivateProperty<UObject* >(&(__Local__45->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__46 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[3], ECastCheckedType::NullAllowed);
	__Local__45->Slot = __Local__44;
	__Local__44->Content = __Local__45;
	__Local__41.Add(__Local__44);
	auto __Local__47 = NewObject<UCanvasPanelSlot>(__Local__40, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_3"), (EObjectFlags)0x00280008);
	__Local__47->LayoutData.Offsets.Left = 60.000000f;
	__Local__47->LayoutData.Offsets.Right = 30.000000f;
	__Local__47->LayoutData.Offsets.Bottom = 45.000000f;
	__Local__47->Parent = __Local__40;
	auto __Local__48 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image"), (EObjectFlags)0x00280008);
	__Local__48->Brush.ImageSize = FVector2D(420.000000, 503.000000);
	auto& __Local__49 = (*(AccessPrivateProperty<UObject* >(&(__Local__48->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__49 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[7], ECastCheckedType::NullAllowed);
	__Local__48->Slot = __Local__47;
	__Local__47->Content = __Local__48;
	__Local__41.Add(__Local__47);
	auto __Local__50 = NewObject<UCanvasPanelSlot>(__Local__40, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_4"), (EObjectFlags)0x00280008);
	__Local__50->LayoutData.Offsets.Top = -50.000000f;
	__Local__50->LayoutData.Offsets.Right = 151.000000f;
	__Local__50->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__50->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__50->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__50->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__50->bAutoSize = true;
	__Local__50->Parent = __Local__40;
	auto __Local__51 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_136"), (EObjectFlags)0x00280008);
	__Local__51->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"83926B0A4416C680CBF2C4A2F514E391\", \"1\")")	);
	__Local__51->Slot = __Local__50;
	__Local__50->Content = __Local__51;
	__Local__41.Add(__Local__50);
	auto __Local__52 = NewObject<UCanvasPanelSlot>(__Local__40, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_1"), (EObjectFlags)0x00280008);
	__Local__52->LayoutData.Offsets.Top = 50.000000f;
	__Local__52->LayoutData.Offsets.Right = 151.000000f;
	__Local__52->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__52->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__52->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__52->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__52->bAutoSize = true;
	__Local__52->Parent = __Local__40;
	auto __Local__53 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_3"), (EObjectFlags)0x00280008);
	__Local__53->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"C6FC071C447CA4D229FE2785E1713666\", \"2000\")")	);
	__Local__53->Slot = __Local__52;
	__Local__52->Content = __Local__53;
	__Local__41.Add(__Local__52);
	auto __Local__54 = NewObject<UCanvasPanelSlot>(__Local__40, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_5"), (EObjectFlags)0x00280008);
	__Local__54->LayoutData.Offsets.Left = -20.000000f;
	__Local__54->LayoutData.Offsets.Top = 60.000000f;
	__Local__54->LayoutData.Offsets.Right = 30.000000f;
	__Local__54->Parent = __Local__40;
	auto __Local__55 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_7"), (EObjectFlags)0x00280008);
	__Local__55->Brush.ImageSize = FVector2D(1018.000000, 1018.000000);
	auto& __Local__56 = (*(AccessPrivateProperty<UObject* >(&(__Local__55->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__56 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
	__Local__55->Slot = __Local__54;
	__Local__54->Content = __Local__55;
	__Local__41.Add(__Local__54);
	__Local__40->Slot = __Local__39;
	__Local__39->Content = __Local__40;
	__Local__2.Add(__Local__39);
	auto __Local__57 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_15"), (EObjectFlags)0x00280008);
	__Local__57->LayoutData.Offsets.Top = -150.000000f;
	__Local__57->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__57->LayoutData.Anchors.Minimum = FVector2D(0.500000, 1.000000);
	__Local__57->LayoutData.Anchors.Maximum = FVector2D(0.500000, 1.000000);
	__Local__57->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__57->Parent = __Local__1;
	auto __Local__58 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("Button2"), (EObjectFlags)0x00280008);
	auto& __Local__59 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__58), UPanelWidget::__PPO__Slots() )));
	__Local__59 = TArray<UPanelSlot*> ();
	__Local__59.Reserve(6);
	auto __Local__60 = NewObject<UCanvasPanelSlot>(__Local__58, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_2"), (EObjectFlags)0x00280008);
	__Local__60->LayoutData.Offsets.Top = -5.000000f;
	__Local__60->LayoutData.Offsets.Bottom = 60.000000f;
	__Local__60->Parent = __Local__58;
	auto __Local__61 = NewObject<UBorder>(__Local__0, UBorder::StaticClass(), TEXT("Border"), (EObjectFlags)0x00280008);
	__Local__61->ContentColorAndOpacity = FLinearColor(0.000000, 0.000000, 0.000000, 1.000000);
	__Local__61->BrushColor = FLinearColor(0.000000, 0.000000, 0.000000, 1.000000);
	__Local__61->Slot = __Local__60;
	__Local__60->Content = __Local__61;
	__Local__59.Add(__Local__60);
	auto __Local__62 = NewObject<UCanvasPanelSlot>(__Local__58, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"), (EObjectFlags)0x00280008);
	__Local__62->LayoutData.Offsets.Left = 10.000000f;
	__Local__62->LayoutData.Offsets.Right = 50.000000f;
	__Local__62->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__62->Parent = __Local__58;
	auto __Local__63 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_1"), (EObjectFlags)0x00280008);
	__Local__63->Brush.ImageSize = FVector2D(600.000000, 600.000000);
	auto& __Local__64 = (*(AccessPrivateProperty<UObject* >(&(__Local__63->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__64 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed);
	__Local__63->Slot = __Local__62;
	__Local__62->Content = __Local__63;
	__Local__59.Add(__Local__62);
	auto __Local__65 = NewObject<UCanvasPanelSlot>(__Local__58, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_3"), (EObjectFlags)0x00280008);
	__Local__65->LayoutData.Offsets.Left = 60.000000f;
	__Local__65->LayoutData.Offsets.Right = 30.000000f;
	__Local__65->LayoutData.Offsets.Bottom = 45.000000f;
	__Local__65->Parent = __Local__58;
	auto __Local__66 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_2"), (EObjectFlags)0x00280008);
	__Local__66->Brush.ImageSize = FVector2D(420.000000, 503.000000);
	auto& __Local__67 = (*(AccessPrivateProperty<UObject* >(&(__Local__66->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__67 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[7], ECastCheckedType::NullAllowed);
	__Local__66->Slot = __Local__65;
	__Local__65->Content = __Local__66;
	__Local__59.Add(__Local__65);
	auto __Local__68 = NewObject<UCanvasPanelSlot>(__Local__58, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_4"), (EObjectFlags)0x00280008);
	__Local__68->LayoutData.Offsets.Top = -50.000000f;
	__Local__68->LayoutData.Offsets.Right = 151.000000f;
	__Local__68->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__68->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__68->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__68->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__68->bAutoSize = true;
	__Local__68->Parent = __Local__58;
	auto __Local__69 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock"), (EObjectFlags)0x00280008);
	__Local__69->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"5284BA93499E50B59F8875BE87BB9126\", \"2\")")	);
	__Local__69->Slot = __Local__68;
	__Local__68->Content = __Local__69;
	__Local__59.Add(__Local__68);
	auto __Local__70 = NewObject<UCanvasPanelSlot>(__Local__58, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_1"), (EObjectFlags)0x00280008);
	__Local__70->LayoutData.Offsets.Top = 50.000000f;
	__Local__70->LayoutData.Offsets.Right = 151.000000f;
	__Local__70->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__70->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__70->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__70->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__70->bAutoSize = true;
	__Local__70->Parent = __Local__58;
	auto __Local__71 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_4"), (EObjectFlags)0x00280008);
	__Local__71->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"B2B07BD147BBCE567715D6A6FBC36EFC\", \"200\")")	);
	__Local__71->Slot = __Local__70;
	__Local__70->Content = __Local__71;
	__Local__59.Add(__Local__70);
	auto __Local__72 = NewObject<UCanvasPanelSlot>(__Local__58, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_6"), (EObjectFlags)0x00280008);
	__Local__72->LayoutData.Offsets.Left = -10.000000f;
	__Local__72->LayoutData.Offsets.Top = 60.000000f;
	__Local__72->LayoutData.Offsets.Right = 30.000000f;
	__Local__72->Parent = __Local__58;
	auto __Local__73 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_6"), (EObjectFlags)0x00280008);
	__Local__73->Brush.ImageSize = FVector2D(1018.000000, 1018.000000);
	auto& __Local__74 = (*(AccessPrivateProperty<UObject* >(&(__Local__73->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__74 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[2], ECastCheckedType::NullAllowed);
	__Local__73->Slot = __Local__72;
	__Local__72->Content = __Local__73;
	__Local__59.Add(__Local__72);
	__Local__58->Slot = __Local__57;
	__Local__57->Content = __Local__58;
	__Local__2.Add(__Local__57);
	auto __Local__75 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_16"), (EObjectFlags)0x00280008);
	__Local__75->LayoutData.Offsets.Left = 150.000000f;
	__Local__75->LayoutData.Offsets.Top = -150.000000f;
	__Local__75->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__75->LayoutData.Anchors.Minimum = FVector2D(0.500000, 1.000000);
	__Local__75->LayoutData.Anchors.Maximum = FVector2D(0.500000, 1.000000);
	__Local__75->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__75->Parent = __Local__1;
	auto __Local__76 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("Button3"), (EObjectFlags)0x00280008);
	auto& __Local__77 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__76), UPanelWidget::__PPO__Slots() )));
	__Local__77 = TArray<UPanelSlot*> ();
	__Local__77.Reserve(4);
	auto __Local__78 = NewObject<UCanvasPanelSlot>(__Local__76, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_2"), (EObjectFlags)0x00280008);
	__Local__78->LayoutData.Offsets.Top = -5.000000f;
	__Local__78->LayoutData.Offsets.Bottom = 60.000000f;
	__Local__78->Parent = __Local__76;
	auto __Local__79 = NewObject<UBorder>(__Local__0, UBorder::StaticClass(), TEXT("Border_1"), (EObjectFlags)0x00280008);
	__Local__79->ContentColorAndOpacity = FLinearColor(0.000000, 0.000000, 0.000000, 1.000000);
	__Local__79->BrushColor = FLinearColor(0.000000, 0.000000, 0.000000, 1.000000);
	__Local__79->Slot = __Local__78;
	__Local__78->Content = __Local__79;
	__Local__77.Add(__Local__78);
	auto __Local__80 = NewObject<UCanvasPanelSlot>(__Local__76, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"), (EObjectFlags)0x00280008);
	__Local__80->LayoutData.Offsets.Left = 10.000000f;
	__Local__80->LayoutData.Offsets.Right = 50.000000f;
	__Local__80->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__80->Parent = __Local__76;
	auto __Local__81 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_3"), (EObjectFlags)0x00280008);
	__Local__81->Brush.ImageSize = FVector2D(600.000000, 600.000000);
	auto& __Local__82 = (*(AccessPrivateProperty<UObject* >(&(__Local__81->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__82 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[1], ECastCheckedType::NullAllowed);
	__Local__81->Slot = __Local__80;
	__Local__80->Content = __Local__81;
	__Local__77.Add(__Local__80);
	auto __Local__83 = NewObject<UCanvasPanelSlot>(__Local__76, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_3"), (EObjectFlags)0x00280008);
	__Local__83->LayoutData.Offsets.Left = 60.000000f;
	__Local__83->LayoutData.Offsets.Right = 30.000000f;
	__Local__83->LayoutData.Offsets.Bottom = 45.000000f;
	__Local__83->Parent = __Local__76;
	auto __Local__84 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_4"), (EObjectFlags)0x00280008);
	__Local__84->Brush.ImageSize = FVector2D(420.000000, 503.000000);
	auto& __Local__85 = (*(AccessPrivateProperty<UObject* >(&(__Local__84->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__85 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[8], ECastCheckedType::NullAllowed);
	__Local__84->Slot = __Local__83;
	__Local__83->Content = __Local__84;
	__Local__77.Add(__Local__83);
	auto __Local__86 = NewObject<UCanvasPanelSlot>(__Local__76, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_4"), (EObjectFlags)0x00280008);
	__Local__86->LayoutData.Offsets.Top = -50.000000f;
	__Local__86->LayoutData.Offsets.Right = 151.000000f;
	__Local__86->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__86->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__86->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__86->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__86->bAutoSize = true;
	__Local__86->Parent = __Local__76;
	auto __Local__87 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_1"), (EObjectFlags)0x00280008);
	__Local__87->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"3DB0C4714237C709B74ACF940F4356C8\", \"3\")")	);
	__Local__87->Slot = __Local__86;
	__Local__86->Content = __Local__87;
	__Local__77.Add(__Local__86);
	__Local__76->Slot = __Local__75;
	__Local__75->Content = __Local__76;
	__Local__2.Add(__Local__75);
	auto __Local__88 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_21"), (EObjectFlags)0x00280008);
	__Local__88->LayoutData.Offsets.Right = 640.000000f;
	__Local__88->LayoutData.Offsets.Bottom = 412.000000f;
	__Local__88->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__88->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__88->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__88->Parent = __Local__1;
	auto __Local__89 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("War"), (EObjectFlags)0x00280008);
	__Local__89->Brush.ImageSize = FVector2D(640.000000, 412.000000);
	auto& __Local__90 = (*(AccessPrivateProperty<UObject* >(&(__Local__89->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__90 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[9], ECastCheckedType::NullAllowed);
	__Local__89->Slot = __Local__88;
	__Local__89->Visibility = ESlateVisibility::Hidden;
	__Local__88->Content = __Local__89;
	__Local__2.Add(__Local__88);
	auto __Local__91 = NewObject<UCanvasPanelSlot>(__Local__1, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_1"), (EObjectFlags)0x00280008);
	__Local__91->LayoutData.Offsets.Left = -147.833542f;
	__Local__91->LayoutData.Offsets.Top = 50.000000f;
	__Local__91->LayoutData.Offsets.Right = 262.634064f;
	__Local__91->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__91->LayoutData.Anchors.Minimum = FVector2D(1.000000, 0.000000);
	__Local__91->LayoutData.Anchors.Maximum = FVector2D(1.000000, 0.000000);
	__Local__91->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__91->Parent = __Local__1;
	auto __Local__92 = NewObject<UCanvasPanel>(__Local__0, UCanvasPanel::StaticClass(), TEXT("Button3_1"), (EObjectFlags)0x00280008);
	auto& __Local__93 = (*(AccessPrivateProperty<TArray<UPanelSlot*> >((__Local__92), UPanelWidget::__PPO__Slots() )));
	__Local__93 = TArray<UPanelSlot*> ();
	__Local__93.Reserve(2);
	auto __Local__94 = NewObject<UCanvasPanelSlot>(__Local__92, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_0"), (EObjectFlags)0x00280008);
	__Local__94->LayoutData.Offsets.Left = 10.000000f;
	__Local__94->LayoutData.Offsets.Right = 50.000000f;
	__Local__94->LayoutData.Offsets.Bottom = 50.000000f;
	__Local__94->Parent = __Local__92;
	auto __Local__95 = NewObject<UImage>(__Local__0, UImage::StaticClass(), TEXT("Image_5"), (EObjectFlags)0x00280008);
	__Local__95->Brush.ImageSize = FVector2D(485.000000, 485.000000);
	auto& __Local__96 = (*(AccessPrivateProperty<UObject* >(&(__Local__95->Brush), FSlateBrush::__PPO__ResourceObject() )));
	__Local__96 = CastChecked<UObject>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->UsedAssets[10], ECastCheckedType::NullAllowed);
	__Local__95->Slot = __Local__94;
	__Local__94->Content = __Local__95;
	__Local__93.Add(__Local__94);
	auto __Local__97 = NewObject<UCanvasPanelSlot>(__Local__92, UCanvasPanelSlot::StaticClass(), TEXT("CanvasPanelSlot_4"), (EObjectFlags)0x00280008);
	__Local__97->LayoutData.Offsets.Left = 30.244530f;
	__Local__97->LayoutData.Offsets.Right = 186.607040f;
	__Local__97->LayoutData.Offsets.Bottom = 40.000000f;
	__Local__97->LayoutData.Anchors.Minimum = FVector2D(0.500000, 0.500000);
	__Local__97->LayoutData.Anchors.Maximum = FVector2D(0.500000, 0.500000);
	__Local__97->LayoutData.Alignment = FVector2D(0.500000, 0.500000);
	__Local__97->Parent = __Local__92;
	auto __Local__98 = NewObject<UTextBlock>(__Local__0, UTextBlock::StaticClass(), TEXT("TextBlock_2"), (EObjectFlags)0x00280008);
	__Local__98->Text = FTextStringHelper::CreateFromBuffer(
TEXT("NSLOCTEXT(\"[59CE4B8E4C4B840026A03ABE83B49D60]\", \"3DB0C4714237C709B74ACF940F4356C8\", \"3\")")	);
	__Local__98->Slot = __Local__97;
	__Local__97->Content = __Local__98;
	__Local__93.Add(__Local__97);
	__Local__92->Slot = __Local__91;
	__Local__91->Content = __Local__92;
	__Local__2.Add(__Local__91);
	__Local__0->RootWidget = __Local__1;
}
PRAGMA_ENABLE_OPTIMIZATION
void UUI_Stats_C__pf3053510930::GetSlotNames(TArray<FName>& SlotNames) const
{
	TArray<FName>  __Local__99;
	SlotNames.Append(__Local__99);
}
void UUI_Stats_C__pf3053510930::InitializeNativeClassData()
{
	TArray<UWidgetAnimation*>  __Local__100;
	TArray<FDelegateRuntimeBinding>  __Local__101;
	__Local__101.AddUninitialized(7);
	FDelegateRuntimeBinding::StaticStruct()->InitializeStruct(__Local__101.GetData(), 7);
	auto& __Local__102 = __Local__101[0];
	__Local__102.ObjectName = FString(TEXT("TextBlock_195"));
	__Local__102.PropertyName = FName(TEXT("Text"));
	__Local__102.FunctionName = FName(TEXT("GetText_0"));
	auto& __Local__103 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__102.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__103 = TArray<FPropertyPathSegment> ();
	__Local__103.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__103.GetData(), 1);
	auto& __Local__104 = __Local__103[0];
	__Local__104.Name = FName(TEXT("Get Population"));
	auto& __Local__105 = __Local__101[1];
	__Local__105.ObjectName = FString(TEXT("TextBlock_250"));
	__Local__105.PropertyName = FName(TEXT("Text"));
	__Local__105.FunctionName = FName(TEXT("GetText_0"));
	auto& __Local__106 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__105.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__106 = TArray<FPropertyPathSegment> ();
	__Local__106.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__106.GetData(), 1);
	auto& __Local__107 = __Local__106[0];
	__Local__107.Name = FName(TEXT("Get Ozone"));
	auto& __Local__108 = __Local__101[2];
	__Local__108.ObjectName = FString(TEXT("TextBlock_326"));
	__Local__108.PropertyName = FName(TEXT("Text"));
	__Local__108.FunctionName = FName(TEXT("GetText_0"));
	auto& __Local__109 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__108.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__109 = TArray<FPropertyPathSegment> ();
	__Local__109.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__109.GetData(), 1);
	auto& __Local__110 = __Local__109[0];
	__Local__110.Name = FName(TEXT("GetResources"));
	auto& __Local__111 = __Local__101[3];
	__Local__111.ObjectName = FString(TEXT("TextBlock_362"));
	__Local__111.PropertyName = FName(TEXT("Text"));
	__Local__111.FunctionName = FName(TEXT("GetText_1"));
	auto& __Local__112 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__111.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__112 = TArray<FPropertyPathSegment> ();
	__Local__112.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__112.GetData(), 1);
	auto& __Local__113 = __Local__112[0];
	__Local__113.Name = FName(TEXT("GetCoal"));
	auto& __Local__114 = __Local__101[4];
	__Local__114.ObjectName = FString(TEXT("TextBlock_401"));
	__Local__114.PropertyName = FName(TEXT("Text"));
	__Local__114.FunctionName = FName(TEXT("GetText_2"));
	auto& __Local__115 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__114.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__115 = TArray<FPropertyPathSegment> ();
	__Local__115.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__115.GetData(), 1);
	auto& __Local__116 = __Local__115[0];
	__Local__116.Name = FName(TEXT("GetSolar"));
	auto& __Local__117 = __Local__101[5];
	__Local__117.ObjectName = FString(TEXT("TextBlock_449"));
	__Local__117.PropertyName = FName(TEXT("Text"));
	__Local__117.FunctionName = FName(TEXT("GetText_3"));
	auto& __Local__118 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__117.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__118 = TArray<FPropertyPathSegment> ();
	__Local__118.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__118.GetData(), 1);
	auto& __Local__119 = __Local__118[0];
	__Local__119.Name = FName(TEXT("GetEnergy"));
	auto& __Local__120 = __Local__101[6];
	__Local__120.ObjectName = FString(TEXT("TextBlock_2"));
	__Local__120.PropertyName = FName(TEXT("Text"));
	__Local__120.FunctionName = FName(TEXT("GetText_0"));
	auto& __Local__121 = (*(AccessPrivateProperty<TArray<FPropertyPathSegment> >(&(__Local__120.SourcePath), FCachedPropertyPath::__PPO__Segments() )));
	__Local__121 = TArray<FPropertyPathSegment> ();
	__Local__121.AddUninitialized(1);
	FPropertyPathSegment::StaticStruct()->InitializeStruct(__Local__121.GetData(), 1);
	auto& __Local__122 = __Local__121[0];
	__Local__122.Name = FName(TEXT("GetText_0"));
	UWidgetBlueprintGeneratedClass::InitializeWidgetStatic(this, GetClass(), false, true, CastChecked<UWidgetTree>(CastChecked<UDynamicClass>(UUI_Stats_C__pf3053510930::StaticClass())->MiscConvertedSubobjects[0]), __Local__100, __Local__101);
}
void UUI_Stats_C__pf3053510930::PreSave(const class ITargetPlatform* TargetPlatform)
{
	Super::PreSave(TargetPlatform);
	TArray<FName> LocalNamedSlots;
	GetSlotNames(LocalNamedSlots);
	RemoveObsoleteBindings(LocalNamedSlots);
}
FText  UUI_Stats_C__pf3053510930::bpf__GetxPopulation__pfT()
{
	FText bpp__ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpv__Population__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
FText  UUI_Stats_C__pf3053510930::bpf__GetxOzone__pfT()
{
	FText bpp__ReturnValue__pf{};
	float bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf{};
	int32 bpfv__CallFunc_Round_ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf = UKismetMathLibrary::Multiply_FloatFloat(bpv__Ozone__pf, 100.000000);
	bpfv__CallFunc_Round_ReturnValue__pf = UKismetMathLibrary::Round(bpfv__CallFunc_Multiply_FloatFloat_ReturnValue__pf);
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpfv__CallFunc_Round_ReturnValue__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
FText  UUI_Stats_C__pf3053510930::bpf__GetResources__pf()
{
	FText bpp__ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpv__Resources__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
FText  UUI_Stats_C__pf3053510930::bpf__GetCoal__pf()
{
	FText bpp__ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpv__Coal__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
FText  UUI_Stats_C__pf3053510930::bpf__GetSolar__pf()
{
	FText bpp__ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpv__Solar__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
FText  UUI_Stats_C__pf3053510930::bpf__GetEnergy__pf()
{
	FText bpp__ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpv__Energy__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
FText  UUI_Stats_C__pf3053510930::bpf__GetText_0__pf()
{
	FText bpp__ReturnValue__pf{};
	FText bpfv__CallFunc_Conv_IntToText_ReturnValue__pf{};
	bpfv__CallFunc_Conv_IntToText_ReturnValue__pf = UKismetTextLibrary::Conv_IntToText(bpv__Time__pf, false, true, 1, 324);
	bpp__ReturnValue__pf = bpfv__CallFunc_Conv_IntToText_ReturnValue__pf;
	return bpp__ReturnValue__pf;
}
PRAGMA_DISABLE_OPTIMIZATION
void UUI_Stats_C__pf3053510930::__StaticDependencies_DirectlyUsedAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{44, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Pop.ICO_Pop 
		{45, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Coal.ICO_Coal 
		{46, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Res.ICO_Res 
		{47, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Sol.ICO_Sol 
		{48, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Ozone.ICO_Ozone 
		{49, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_NRG.ICO_NRG 
		{50, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Warning.ICO_Warning 
		{51, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Up.ICO_Up 
		{52, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Down.ICO_Down 
		{53, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/war.war 
		{43, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Texture2D /Game/Icons/ICO_Clock.ICO_Clock 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
PRAGMA_DISABLE_OPTIMIZATION
void UUI_Stats_C__pf3053510930::__StaticDependenciesAssets(TArray<FBlueprintDependencyData>& AssetsToLoad)
{
	__StaticDependencies_DirectlyUsedAssets(AssetsToLoad);
	const FCompactBlueprintDependencyData LocCompactBlueprintDependencyData[] =
	{
		{42, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Font /Engine/EngineFonts/Roboto.Roboto 
		{17, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.UserWidget 
		{35, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetTextLibrary 
		{18, FBlueprintDependencyType(false, true, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/Engine.KismetMathLibrary 
		{33, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.Image 
		{34, FBlueprintDependencyType(true, false, false, false), FBlueprintDependencyType(false, false, false, false)},  //  Class /Script/UMG.TextBlock 
	};
	for(const FCompactBlueprintDependencyData& CompactData : LocCompactBlueprintDependencyData)
	{
		AssetsToLoad.Add(FBlueprintDependencyData(F__NativeDependencies::Get(CompactData.ObjectRefIndex), CompactData));
	}
}
PRAGMA_ENABLE_OPTIMIZATION
struct FRegisterHelper__UUI_Stats_C__pf3053510930
{
	FRegisterHelper__UUI_Stats_C__pf3053510930()
	{
		FConvertedBlueprintsDependencies::Get().RegisterConvertedClass(TEXT("/Game/Blueprints/UI/UI_Stats"), &UUI_Stats_C__pf3053510930::__StaticDependenciesAssets);
	}
	static FRegisterHelper__UUI_Stats_C__pf3053510930 Instance;
};
FRegisterHelper__UUI_Stats_C__pf3053510930 FRegisterHelper__UUI_Stats_C__pf3053510930::Instance;
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
