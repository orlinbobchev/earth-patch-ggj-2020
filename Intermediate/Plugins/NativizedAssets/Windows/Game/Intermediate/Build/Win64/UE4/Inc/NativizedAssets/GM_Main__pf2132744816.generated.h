// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NATIVIZEDASSETS_GM_Main__pf2132744816_generated_h
#error "GM_Main__pf2132744816.generated.h already included, missing '#pragma once' in GM_Main__pf2132744816.h"
#endif
#define NATIVIZEDASSETS_GM_Main__pf2132744816_generated_h

#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_SPARSE_DATA
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_RPC_WRAPPERS
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGM_Main_C__pf2132744816(); \
	friend struct Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(AGM_Main_C__pf2132744816, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/GM_Main"), NO_API) \
	DECLARE_SERIALIZER(AGM_Main_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGM_Main_C__pf2132744816(); \
	friend struct Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(AGM_Main_C__pf2132744816, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/GM_Main"), NO_API) \
	DECLARE_SERIALIZER(AGM_Main_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGM_Main_C__pf2132744816(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGM_Main_C__pf2132744816) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGM_Main_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGM_Main_C__pf2132744816); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGM_Main_C__pf2132744816(AGM_Main_C__pf2132744816&&); \
	NO_API AGM_Main_C__pf2132744816(const AGM_Main_C__pf2132744816&); \
public:


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGM_Main_C__pf2132744816(AGM_Main_C__pf2132744816&&); \
	NO_API AGM_Main_C__pf2132744816(const AGM_Main_C__pf2132744816&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGM_Main_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGM_Main_C__pf2132744816); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGM_Main_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_PRIVATE_PROPERTY_OFFSET
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_8_PROLOG
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_RPC_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_INCLASS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_INCLASS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h_12_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NATIVIZEDASSETS_API UClass* StaticClass<class AGM_Main_C__pf2132744816>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_GM_Main__pf2132744816_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
