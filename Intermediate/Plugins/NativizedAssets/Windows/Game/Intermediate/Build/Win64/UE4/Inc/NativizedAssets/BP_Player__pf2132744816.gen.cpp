// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Public/BP_Player__pf2132744816.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBP_Player__pf2132744816() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABP_Player_C__pf2132744816_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABP_Player_C__pf2132744816();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABP_Earth_C__pf2132744816_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_UUI_Stats_C__pf3053510930_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	static FName NAME_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ABP_Player_C__pf2132744816::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void ABP_Player_C__pf2132744816::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		BP_Player_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf),&Parms);
	}
	void ABP_Player_C__pf2132744816::StaticRegisterNativesABP_Player_C__pf2132744816()
	{
		UClass* Class = ABP_Player_C__pf2132744816::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ExecuteUbergraph_BP_Player_0", &ABP_Player_C__pf2132744816::execbpf__ExecuteUbergraph_BP_Player__pf_0 },
			{ "Game Over", &ABP_Player_C__pf2132744816::execbpf__GamexOver__pfT },
			{ "InpActEvt_AddCoal_K2Node_InputActionEvent_2", &ABP_Player_C__pf2132744816::execbpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf },
			{ "InpActEvt_AddSolar_K2Node_InputActionEvent_3", &ABP_Player_C__pf2132744816::execbpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf },
			{ "InpActEvt_RemoveCoal_K2Node_InputActionEvent_1", &ABP_Player_C__pf2132744816::execbpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf },
			{ "InpActEvt_ShowOzone_K2Node_InputActionEvent_0", &ABP_Player_C__pf2132744816::execbpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf },
			{ "InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3", &ABP_Player_C__pf2132744816::execbpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf },
			{ "InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4", &ABP_Player_C__pf2132744816::execbpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf },
			{ "InpAxisEvt_Zoom_K2Node_InputAxisEvent_5", &ABP_Player_C__pf2132744816::execbpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf },
			{ "ReceiveBeginPlay", &ABP_Player_C__pf2132744816::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveTick", &ABP_Player_C__pf2132744816::execbpf__ReceiveTick__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__ExecuteUbergraph_BP_Player__pf_0_Parms
		{
			int32 bpp__EntryPoint__pf;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpp__EntryPoint__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::NewProp_bpp__EntryPoint__pf = { "bpp__EntryPoint__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__ExecuteUbergraph_BP_Player__pf_0_Parms, bpp__EntryPoint__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::NewProp_bpp__EntryPoint__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "ExecuteUbergraph_BP_Player_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "ExecuteUbergraph_BP_Player_0", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__ExecuteUbergraph_BP_Player__pf_0_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ExecuteUbergraph_BP_Player_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "Game Over" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "Game Over", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Game Over") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::NewProp_bpp__Key__pf = { "bpp__Key__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "InpActEvt_AddCoal_K2Node_InputActionEvent_2" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "InpActEvt_AddCoal_K2Node_InputActionEvent_2", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_AddCoal_K2Node_InputActionEvent_2") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::NewProp_bpp__Key__pf = { "bpp__Key__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "InpActEvt_AddSolar_K2Node_InputActionEvent_3" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "InpActEvt_AddSolar_K2Node_InputActionEvent_3", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_AddSolar_K2Node_InputActionEvent_3") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::NewProp_bpp__Key__pf = { "bpp__Key__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "InpActEvt_RemoveCoal_K2Node_InputActionEvent_1" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "InpActEvt_RemoveCoal_K2Node_InputActionEvent_1", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_RemoveCoal_K2Node_InputActionEvent_1") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Parms
		{
			FKey bpp__Key__pf;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bpp__Key__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::NewProp_bpp__Key__pf = { "bpp__Key__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Parms, bpp__Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::NewProp_bpp__Key__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "InpActEvt_ShowOzone_K2Node_InputActionEvent_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "InpActEvt_ShowOzone_K2Node_InputActionEvent_0", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpActEvt_ShowOzone_K2Node_InputActionEvent_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Parms
		{
			float bpp__AxisValue__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__AxisValue__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::NewProp_bpp__AxisValue__pf = { "bpp__AxisValue__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Parms, bpp__AxisValue__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::NewProp_bpp__AxisValue__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Parms
		{
			float bpp__AxisValue__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__AxisValue__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::NewProp_bpp__AxisValue__pf = { "bpp__AxisValue__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Parms, bpp__AxisValue__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::NewProp_bpp__AxisValue__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics
	{
		struct BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Parms
		{
			float bpp__AxisValue__pf;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__AxisValue__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::NewProp_bpp__AxisValue__pf = { "bpp__AxisValue__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Parms, bpp__AxisValue__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::NewProp_bpp__AxisValue__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "InpAxisEvt_Zoom_K2Node_InputAxisEvent_5" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "InpAxisEvt_Zoom_K2Node_InputAxisEvent_5", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("InpAxisEvt_Zoom_K2Node_InputAxisEvent_5") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event when play begins for this actor. */" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "ReceiveBeginPlay", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient, (EFunctionFlags)0x00020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { "bpp__DeltaSeconds__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_Player_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event called every frame, if ticking is enabled */" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame, if ticking is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_Player_C__pf2132744816, nullptr, "ReceiveTick", nullptr, nullptr, sizeof(BP_Player_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_Player_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABP_Player_C__pf2132744816_NoRegister()
	{
		return ABP_Player_C__pf2132744816::StaticClass();
	}
	struct Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Select_Default__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Select_Default__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__Temp_bool_Variable_1__pf_MetaData[];
#endif
		static void NewProp_b0l__Temp_bool_Variable_1__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__Temp_bool_Variable_1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__Temp_bool_Variable__pf_MetaData[];
#endif
		static void NewProp_b0l__Temp_bool_Variable__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__Temp_bool_Variable__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__Temp_bool_IsClosed_Variable__pf_MetaData[];
#endif
		static void NewProp_b0l__Temp_bool_IsClosed_Variable__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__Temp_bool_IsClosed_Variable__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key_3__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key_3__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key_2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key_2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key_1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key_1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__K2Node_InputActionEvent_Key__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf_MetaData[];
#endif
		static void NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakRotator_Yaw__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakRotator_Yaw__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakRotator_Pitch__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakRotator_Pitch__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_BreakRotator_Roll__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__CallFunc_BreakRotator_Roll__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputAxisEvent_AxisValue_1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_InputAxisEvent_AxisValue_1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_InputAxisEvent_AxisValue_2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_InputAxisEvent_AxisValue_2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Earth__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Earth__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__UI__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__UI__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ResourceManager__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__ResourceManager__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ZoomAmount__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ZoomAmount__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ZoomFarthest__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ZoomFarthest__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ZoomClosest__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ZoomClosest__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Scene__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Scene__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__SpringArm__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__SpringArm__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Camera__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Camera__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ExecuteUbergraph_BP_Player__pf_0, "ExecuteUbergraph_BP_Player_0" }, // 1992937414
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__GamexOver__pfT, "Game Over" }, // 532926951
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf, "InpActEvt_AddCoal_K2Node_InputActionEvent_2" }, // 3737035005
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf, "InpActEvt_AddSolar_K2Node_InputActionEvent_3" }, // 102016198
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf, "InpActEvt_RemoveCoal_K2Node_InputActionEvent_1" }, // 2878363287
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf, "InpActEvt_ShowOzone_K2Node_InputActionEvent_0" }, // 620358670
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf, "InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3" }, // 807578523
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf, "InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4" }, // 2643693223
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf, "InpAxisEvt_Zoom_K2Node_InputAxisEvent_5" }, // 272422917
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 1140867222
		{ &Z_Construct_UFunction_ABP_Player_C__pf2132744816_bpf__ReceiveTick__pf, "ReceiveTick" }, // 877134476
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "BP_Player__pf2132744816.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "BP_Player_C" },
		{ "ReplaceConverted", "/Game/Blueprints/BP_Player.BP_Player_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Select_Default__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_Select_Default" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Select_Default__pf = { "K2Node_Select_Default", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_Select_Default__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Select_Default__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Select_Default__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable_1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "Temp_bool_Variable_1" },
	};
#endif
	void Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable_1__pf_SetBit(void* Obj)
	{
		((ABP_Player_C__pf2132744816*)Obj)->b0l__Temp_bool_Variable_1__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable_1__pf = { "Temp_bool_Variable_1", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient, 1, sizeof(bool), sizeof(ABP_Player_C__pf2132744816), &Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable_1__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable_1__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable_1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "Temp_bool_Variable" },
	};
#endif
	void Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable__pf_SetBit(void* Obj)
	{
		((ABP_Player_C__pf2132744816*)Obj)->b0l__Temp_bool_Variable__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable__pf = { "Temp_bool_Variable", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient, 1, sizeof(bool), sizeof(ABP_Player_C__pf2132744816), &Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_IsClosed_Variable__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "Temp_bool_IsClosed_Variable" },
	};
#endif
	void Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_IsClosed_Variable__pf_SetBit(void* Obj)
	{
		((ABP_Player_C__pf2132744816*)Obj)->b0l__Temp_bool_IsClosed_Variable__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_IsClosed_Variable__pf = { "Temp_bool_IsClosed_Variable", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient, 1, sizeof(bool), sizeof(ABP_Player_C__pf2132744816), &Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_IsClosed_Variable__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_IsClosed_Variable__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_IsClosed_Variable__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_3__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key_3" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_3__pf = { "K2Node_InputActionEvent_Key_3", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_InputActionEvent_Key_3__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_3__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_3__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_2__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key_2" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_2__pf = { "K2Node_InputActionEvent_Key_2", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_InputActionEvent_Key_2__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_2__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key_1" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_1__pf = { "K2Node_InputActionEvent_Key_1", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_InputActionEvent_Key_1__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_1__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf = { "K2Node_Event_DeltaSeconds", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_InputActionEvent_Key" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf = { "K2Node_InputActionEvent_Key", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_InputActionEvent_Key__pf), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "Temp_bool_Has_Been_Initd_Variable" },
	};
#endif
	void Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf_SetBit(void* Obj)
	{
		((ABP_Player_C__pf2132744816*)Obj)->b0l__Temp_bool_Has_Been_Initd_Variable__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf = { "Temp_bool_Has_Been_Initd_Variable", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient, 1, sizeof(bool), sizeof(ABP_Player_C__pf2132744816), &Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Yaw__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "CallFunc_BreakRotator_Yaw" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Yaw__pf = { "CallFunc_BreakRotator_Yaw", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__CallFunc_BreakRotator_Yaw__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Yaw__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Yaw__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Pitch__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "CallFunc_BreakRotator_Pitch" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Pitch__pf = { "CallFunc_BreakRotator_Pitch", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__CallFunc_BreakRotator_Pitch__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Pitch__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Pitch__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Roll__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "CallFunc_BreakRotator_Roll" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Roll__pf = { "CallFunc_BreakRotator_Roll", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__CallFunc_BreakRotator_Roll__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Roll__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Roll__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "CallFunc_K2_AddRelativeRotation_SweepHitResult_1" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf = { "CallFunc_K2_AddRelativeRotation_SweepHitResult_1", nullptr, (EPropertyFlags)0x0010008000202000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "CallFunc_K2_AddRelativeRotation_SweepHitResult" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf = { "CallFunc_K2_AddRelativeRotation_SweepHitResult", nullptr, (EPropertyFlags)0x0010008000202000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_InputAxisEvent_AxisValue" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf = { "K2Node_InputAxisEvent_AxisValue", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_InputAxisEvent_AxisValue__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_1__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_InputAxisEvent_AxisValue_1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_1__pf = { "K2Node_InputAxisEvent_AxisValue_1", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_InputAxisEvent_AxisValue_1__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_1__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_2__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_InputAxisEvent_AxisValue_2" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_2__pf = { "K2Node_InputAxisEvent_AxisValue_2", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, b0l__K2Node_InputAxisEvent_AxisValue_2__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_2__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Earth__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Earth" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Earth" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Earth__pf = { "Earth", nullptr, (EPropertyFlags)0x0010000000000805, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__Earth__pf), Z_Construct_UClass_ABP_Earth_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Earth__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Earth__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__UI__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "UI" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "UI" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__UI__pf = { "UI", nullptr, (EPropertyFlags)0x001000000009000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__UI__pf), Z_Construct_UClass_UUI_Stats_C__pf3053510930_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__UI__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__UI__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ResourceManager__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Resource Manager" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "ResourceManager" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ResourceManager__pf = { "ResourceManager", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__ResourceManager__pf), Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ResourceManager__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ResourceManager__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomAmount__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Zoom Amount" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "ZoomAmount" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomAmount__pf = { "ZoomAmount", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__ZoomAmount__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomAmount__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomAmount__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomFarthest__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Zoom Farthest" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "ZoomFarthest" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomFarthest__pf = { "ZoomFarthest", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__ZoomFarthest__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomFarthest__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomFarthest__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomClosest__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Zoom Closest" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "ZoomClosest" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomClosest__pf = { "ZoomClosest", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__ZoomClosest__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomClosest__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomClosest__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Scene__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "Scene" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Scene__pf = { "Scene", nullptr, (EPropertyFlags)0x001000040008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__Scene__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Scene__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Scene__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__SpringArm__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "SpringArm" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__SpringArm__pf = { "SpringArm", nullptr, (EPropertyFlags)0x001000040008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__SpringArm__pf), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__SpringArm__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__SpringArm__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Camera__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/BP_Player__pf2132744816.h" },
		{ "OverrideNativeName", "Camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Camera__pf = { "Camera", nullptr, (EPropertyFlags)0x001000040008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_Player_C__pf2132744816, bpv__Camera__pf), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Camera__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Camera__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Select_Default__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable_1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Variable__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_IsClosed_Variable__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_3__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key_1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputActionEvent_Key__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__Temp_bool_Has_Been_Initd_Variable__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Yaw__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Pitch__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_BreakRotator_Roll__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult_1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__CallFunc_K2_AddRelativeRotation_SweepHitResult__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_b0l__K2Node_InputAxisEvent_AxisValue_2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Earth__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__UI__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ResourceManager__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomAmount__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomFarthest__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__ZoomClosest__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Scene__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__SpringArm__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::NewProp_bpv__Camera__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABP_Player_C__pf2132744816>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::ClassParams = {
		&ABP_Player_C__pf2132744816::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABP_Player_C__pf2132744816()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/BP_Player"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("BP_Player_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ABP_Player_C__pf2132744816, TEXT("BP_Player_C"), 3391420912);
	template<> NATIVIZEDASSETS_API UClass* StaticClass<ABP_Player_C__pf2132744816>()
	{
		return ABP_Player_C__pf2132744816::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABP_Player_C__pf2132744816(Z_Construct_UClass_ABP_Player_C__pf2132744816, &ABP_Player_C__pf2132744816::StaticClass, TEXT("/Game/Blueprints/BP_Player"), TEXT("BP_Player_C"), true, TEXT("/Game/Blueprints/BP_Player"), TEXT("/Game/Blueprints/BP_Player.BP_Player_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABP_Player_C__pf2132744816);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
