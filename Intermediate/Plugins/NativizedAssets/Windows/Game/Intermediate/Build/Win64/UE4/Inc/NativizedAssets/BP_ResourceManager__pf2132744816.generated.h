// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NATIVIZEDASSETS_BP_ResourceManager__pf2132744816_generated_h
#error "BP_ResourceManager__pf2132744816.generated.h already included, missing '#pragma once' in BP_ResourceManager__pf2132744816.h"
#endif
#define NATIVIZEDASSETS_BP_ResourceManager__pf2132744816_generated_h

#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_SPARSE_DATA
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RemoveCoalPlant__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RemoveCoalPlant__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddCoalPlant__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddCoalPlant__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddSolarPlant__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddSolarPlant__pf(); \
		P_NATIVE_END; \
	}


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__RemoveCoalPlant__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__RemoveCoalPlant__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddCoalPlant__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddCoalPlant__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__AddSolarPlant__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__AddSolarPlant__pf(); \
		P_NATIVE_END; \
	}


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_EVENT_PARMS \
	struct BP_ResourceManager_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms \
	{ \
		float bpp__DeltaSeconds__pf; \
	};


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf); \



#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABP_ResourceManager_C__pf2132744816(); \
	friend struct Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(ABP_ResourceManager_C__pf2132744816, ATextRenderActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/BP_ResourceManager"), NO_API) \
	DECLARE_SERIALIZER(ABP_ResourceManager_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_INCLASS \
private: \
	static void StaticRegisterNativesABP_ResourceManager_C__pf2132744816(); \
	friend struct Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(ABP_ResourceManager_C__pf2132744816, ATextRenderActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/BP_ResourceManager"), NO_API) \
	DECLARE_SERIALIZER(ABP_ResourceManager_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABP_ResourceManager_C__pf2132744816(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABP_ResourceManager_C__pf2132744816) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABP_ResourceManager_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABP_ResourceManager_C__pf2132744816); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABP_ResourceManager_C__pf2132744816(ABP_ResourceManager_C__pf2132744816&&); \
	NO_API ABP_ResourceManager_C__pf2132744816(const ABP_ResourceManager_C__pf2132744816&); \
public:


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABP_ResourceManager_C__pf2132744816(ABP_ResourceManager_C__pf2132744816&&); \
	NO_API ABP_ResourceManager_C__pf2132744816(const ABP_ResourceManager_C__pf2132744816&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABP_ResourceManager_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABP_ResourceManager_C__pf2132744816); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABP_ResourceManager_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_PRIVATE_PROPERTY_OFFSET
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_5_PROLOG \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_EVENT_PARMS


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_RPC_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_CALLBACK_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_INCLASS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_CALLBACK_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_INCLASS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h_9_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NATIVIZEDASSETS_API UClass* StaticClass<class ABP_ResourceManager_C__pf2132744816>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_ResourceManager__pf2132744816_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
