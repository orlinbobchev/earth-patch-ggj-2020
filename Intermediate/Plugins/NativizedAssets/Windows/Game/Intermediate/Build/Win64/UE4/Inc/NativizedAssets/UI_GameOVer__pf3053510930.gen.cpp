// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Public/UI_GameOVer__pf3053510930.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUI_GameOVer__pf3053510930() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_UUI_GameOVer_C__pf3053510930();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf();
	UMG_API UClass* Z_Construct_UClass_UTextBlock_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UImage_NoRegister();
// End Cross Module References
	void UUI_GameOVer_C__pf3053510930::StaticRegisterNativesUUI_GameOVer_C__pf3053510930()
	{
		UClass* Class = UUI_GameOVer_C__pf3053510930::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetText_0", &UUI_GameOVer_C__pf3053510930::execbpf__GetText_0__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics
	{
		struct UI_GameOVer_C__pf3053510930_eventbpf__GetText_0__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_GameOVer_C__pf3053510930_eventbpf__GetText_0__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_GameOVer__pf3053510930.h" },
		{ "OverrideNativeName", "GetText_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_GameOVer_C__pf3053510930, nullptr, "GetText_0", nullptr, nullptr, sizeof(UI_GameOVer_C__pf3053510930_eventbpf__GetText_0__pf_Parms), Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf()
	{
		UObject* Outer = Z_Construct_UClass_UUI_GameOVer_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetText_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_NoRegister()
	{
		return UUI_GameOVer_C__pf3053510930::StaticClass();
	}
	struct Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Time__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__Time__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_126__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_126__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_40__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_40__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_27__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_27__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUI_GameOVer_C__pf3053510930_bpf__GetText_0__pf, "GetText_0" }, // 634263824
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "UI_GameOVer__pf3053510930.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/UI_GameOVer__pf3053510930.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "UI_GameOVer_C" },
		{ "ReplaceConverted", "/Game/Blueprints/UI/UI_GameOVer.UI_GameOVer_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Time__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Time" },
		{ "ModuleRelativePath", "Public/UI_GameOVer__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Time" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Time__pf = { "Time", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_GameOVer_C__pf3053510930, bpv__Time__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Time__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Time__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__TextBlock_126__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_126" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_GameOVer__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_126" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__TextBlock_126__pf = { "TextBlock_126", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_GameOVer_C__pf3053510930, bpv__TextBlock_126__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__TextBlock_126__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__TextBlock_126__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_40__pf_MetaData[] = {
		{ "Category", "UI_GameOVer" },
		{ "DisplayName", "Image_40" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_GameOVer__pf3053510930.h" },
		{ "OverrideNativeName", "Image_40" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_40__pf = { "Image_40", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_GameOVer_C__pf3053510930, bpv__Image_40__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_40__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_40__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_27__pf_MetaData[] = {
		{ "Category", "UI_GameOVer" },
		{ "DisplayName", "Image_27" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_GameOVer__pf3053510930.h" },
		{ "OverrideNativeName", "Image_27" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_27__pf = { "Image_27", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_GameOVer_C__pf3053510930, bpv__Image_27__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_27__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_27__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Time__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__TextBlock_126__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_40__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::NewProp_bpv__Image_27__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUI_GameOVer_C__pf3053510930>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::ClassParams = {
		&UUI_GameOVer_C__pf3053510930::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::PropPointers),
		0,
		0x00A010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUI_GameOVer_C__pf3053510930()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/UI/UI_GameOVer"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("UI_GameOVer_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(UUI_GameOVer_C__pf3053510930, TEXT("UI_GameOVer_C"), 2793362813);
	template<> NATIVIZEDASSETS_API UClass* StaticClass<UUI_GameOVer_C__pf3053510930>()
	{
		return UUI_GameOVer_C__pf3053510930::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUI_GameOVer_C__pf3053510930(Z_Construct_UClass_UUI_GameOVer_C__pf3053510930, &UUI_GameOVer_C__pf3053510930::StaticClass, TEXT("/Game/Blueprints/UI/UI_GameOVer"), TEXT("UI_GameOVer_C"), true, TEXT("/Game/Blueprints/UI/UI_GameOVer"), TEXT("/Game/Blueprints/UI/UI_GameOVer.UI_GameOVer_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUI_GameOVer_C__pf3053510930);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
