// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Public/GM_Main__pf2132744816.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGM_Main__pf2132744816() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AGM_Main_C__pf2132744816_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_AGM_Main_C__pf2132744816();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABP_Earth_C__pf2132744816_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	void AGM_Main_C__pf2132744816::StaticRegisterNativesAGM_Main_C__pf2132744816()
	{
	}
	UClass* Z_Construct_UClass_AGM_Main_C__pf2132744816_NoRegister()
	{
		return AGM_Main_C__pf2132744816::StaticClass();
	}
	struct Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__OzoneMaterial__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__OzoneMaterial__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__OzoneIntensity__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__OzoneIntensity__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Earth__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Earth__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__DefaultSceneRoot__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__DefaultSceneRoot__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "GM_Main__pf2132744816.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/GM_Main__pf2132744816.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "GM_Main_C" },
		{ "ReplaceConverted", "/Game/Blueprints/GM_Main.GM_Main_C" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneMaterial__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Ozone Material" },
		{ "ModuleRelativePath", "Public/GM_Main__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "OzoneMaterial" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneMaterial__pf = { "OzoneMaterial", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(AGM_Main_C__pf2132744816, bpv__OzoneMaterial__pf), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneMaterial__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneMaterial__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneIntensity__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Ozone Intensity" },
		{ "ModuleRelativePath", "Public/GM_Main__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "OzoneIntensity" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneIntensity__pf = { "OzoneIntensity", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(AGM_Main_C__pf2132744816, bpv__OzoneIntensity__pf), METADATA_PARAMS(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneIntensity__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneIntensity__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__Earth__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Earth" },
		{ "ModuleRelativePath", "Public/GM_Main__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Earth" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__Earth__pf = { "Earth", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(AGM_Main_C__pf2132744816, bpv__Earth__pf), Z_Construct_UClass_ABP_Earth_C__pf2132744816_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__Earth__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__Earth__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GM_Main__pf2132744816.h" },
		{ "OverrideNativeName", "DefaultSceneRoot" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf = { "DefaultSceneRoot", nullptr, (EPropertyFlags)0x001000040008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(AGM_Main_C__pf2132744816, bpv__DefaultSceneRoot__pf), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneMaterial__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__OzoneIntensity__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__Earth__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::NewProp_bpv__DefaultSceneRoot__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGM_Main_C__pf2132744816>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::ClassParams = {
		&AGM_Main_C__pf2132744816::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::PropPointers),
		0,
		0x008002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGM_Main_C__pf2132744816()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/GM_Main"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("GM_Main_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGM_Main_C__pf2132744816_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(AGM_Main_C__pf2132744816, TEXT("GM_Main_C"), 3280560506);
	template<> NATIVIZEDASSETS_API UClass* StaticClass<AGM_Main_C__pf2132744816>()
	{
		return AGM_Main_C__pf2132744816::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGM_Main_C__pf2132744816(Z_Construct_UClass_AGM_Main_C__pf2132744816, &AGM_Main_C__pf2132744816::StaticClass, TEXT("/Game/Blueprints/GM_Main"), TEXT("GM_Main_C"), true, TEXT("/Game/Blueprints/GM_Main"), TEXT("/Game/Blueprints/GM_Main.GM_Main_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGM_Main_C__pf2132744816);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
