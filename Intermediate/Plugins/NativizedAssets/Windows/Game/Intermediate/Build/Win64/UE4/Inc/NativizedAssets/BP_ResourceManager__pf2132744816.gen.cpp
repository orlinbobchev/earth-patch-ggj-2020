// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Public/BP_ResourceManager__pf2132744816.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBP_ResourceManager__pf2132744816() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816();
	ENGINE_API UClass* Z_Construct_UClass_ATextRenderActor();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf();
// End Cross Module References
	static FName NAME_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf = FName(TEXT("ReceiveBeginPlay"));
	void ABP_ResourceManager_C__pf2132744816::eventbpf__ReceiveBeginPlay__pf()
	{
		ProcessEvent(FindFunctionChecked(NAME_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf),NULL);
	}
	static FName NAME_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf = FName(TEXT("ReceiveTick"));
	void ABP_ResourceManager_C__pf2132744816::eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf)
	{
		BP_ResourceManager_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms Parms;
		Parms.bpp__DeltaSeconds__pf=bpp__DeltaSeconds__pf;
		ProcessEvent(FindFunctionChecked(NAME_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf),&Parms);
	}
	void ABP_ResourceManager_C__pf2132744816::StaticRegisterNativesABP_ResourceManager_C__pf2132744816()
	{
		UClass* Class = ABP_ResourceManager_C__pf2132744816::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddCoalPlant", &ABP_ResourceManager_C__pf2132744816::execbpf__AddCoalPlant__pf },
			{ "AddSolarPlant", &ABP_ResourceManager_C__pf2132744816::execbpf__AddSolarPlant__pf },
			{ "ReceiveBeginPlay", &ABP_ResourceManager_C__pf2132744816::execbpf__ReceiveBeginPlay__pf },
			{ "ReceiveTick", &ABP_ResourceManager_C__pf2132744816::execbpf__ReceiveTick__pf },
			{ "RemoveCoalPlant", &ABP_ResourceManager_C__pf2132744816::execbpf__RemoveCoalPlant__pf },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "OverrideNativeName", "AddCoalPlant" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816, nullptr, "AddCoalPlant", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("AddCoalPlant") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "OverrideNativeName", "AddSolarPlant" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816, nullptr, "AddSolarPlant", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("AddSolarPlant") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event when play begins for this actor. */" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "BeginPlay" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "OverrideNativeName", "ReceiveBeginPlay" },
		{ "ToolTip", "Event when play begins for this actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816, nullptr, "ReceiveBeginPlay", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient, (EFunctionFlags)0x00020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveBeginPlay") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpp__DeltaSeconds__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf = { "bpp__DeltaSeconds__pf", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(BP_ResourceManager_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms, bpp__DeltaSeconds__pf), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::NewProp_bpp__DeltaSeconds__pf,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Event called every frame, if ticking is enabled */" },
		{ "CppFromBpEvent", "" },
		{ "DisplayName", "Tick" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "OverrideNativeName", "ReceiveTick" },
		{ "ToolTip", "Event called every frame, if ticking is enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816, nullptr, "ReceiveTick", nullptr, nullptr, sizeof(BP_ResourceManager_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms), Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x00020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("ReceiveTick") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "OverrideNativeName", "RemoveCoalPlant" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816, nullptr, "RemoveCoalPlant", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf()
	{
		UObject* Outer = Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("RemoveCoalPlant") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_NoRegister()
	{
		return ABP_ResourceManager_C__pf2132744816::StaticClass();
	}
	struct Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_b0l__K2Node_Event_DeltaSeconds__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_b0l__Temp_int_Variable__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_b0l__Temp_int_Variable__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__hasWar__pf_MetaData[];
#endif
		static void NewProp_bpv__hasWar__pf_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bpv__hasWar__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ozonePercent__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__ozonePercent__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__energy__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__energy__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__solarPlant__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__solarPlant__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__coalPlant__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__coalPlant__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__resourseCount__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__resourseCount__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__ozonCount__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__ozonCount__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__growthRate__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__growthRate__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__people__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__people__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__time__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__time__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ATextRenderActor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddCoalPlant__pf, "AddCoalPlant" }, // 103344721
		{ &Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__AddSolarPlant__pf, "AddSolarPlant" }, // 1177300430
		{ &Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveBeginPlay__pf, "ReceiveBeginPlay" }, // 4271277532
		{ &Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__ReceiveTick__pf, "ReceiveTick" }, // 163854243
		{ &Z_Construct_UFunction_ABP_ResourceManager_C__pf2132744816_bpf__RemoveCoalPlant__pf, "RemoveCoalPlant" }, // 11461887
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Collision Attachment Actor" },
		{ "IncludePath", "BP_ResourceManager__pf2132744816.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "BP_ResourceManager_C" },
		{ "ReplaceConverted", "/Game/Blueprints/BP_ResourceManager.BP_ResourceManager_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "OverrideNativeName", "K2Node_Event_DeltaSeconds" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf = { "K2Node_Event_DeltaSeconds", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, b0l__K2Node_Event_DeltaSeconds__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__Temp_int_Variable__pf_MetaData[] = {
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "OverrideNativeName", "Temp_int_Variable" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__Temp_int_Variable__pf = { "Temp_int_Variable", nullptr, (EPropertyFlags)0x0010000000202000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, b0l__Temp_int_Variable__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__Temp_int_Variable__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__Temp_int_Variable__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__hasWar__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Has War" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "hasWar" },
	};
#endif
	void Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__hasWar__pf_SetBit(void* Obj)
	{
		((ABP_ResourceManager_C__pf2132744816*)Obj)->bpv__hasWar__pf = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__hasWar__pf = { "hasWar", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient, 1, sizeof(bool), sizeof(ABP_ResourceManager_C__pf2132744816), &Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__hasWar__pf_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__hasWar__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__hasWar__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonePercent__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Ozone Percent" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "ozonePercent" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonePercent__pf = { "ozonePercent", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__ozonePercent__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonePercent__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonePercent__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__energy__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Energy" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "energy" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__energy__pf = { "energy", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__energy__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__energy__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__energy__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__solarPlant__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Solar Plant" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "solarPlant" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__solarPlant__pf = { "solarPlant", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__solarPlant__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__solarPlant__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__solarPlant__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__coalPlant__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Coal Plant" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "coalPlant" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__coalPlant__pf = { "coalPlant", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__coalPlant__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__coalPlant__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__coalPlant__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__resourseCount__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Resourse Count" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "resourseCount" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__resourseCount__pf = { "resourseCount", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__resourseCount__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__resourseCount__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__resourseCount__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonCount__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Ozon Count" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "ozonCount" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonCount__pf = { "ozonCount", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__ozonCount__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonCount__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonCount__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__growthRate__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Growth Rate" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "growthRate" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__growthRate__pf = { "growthRate", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__growthRate__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__growthRate__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__growthRate__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__people__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "People" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "people" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__people__pf = { "people", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__people__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__people__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__people__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__time__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Time" },
		{ "ModuleRelativePath", "Public/BP_ResourceManager__pf2132744816.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "time" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__time__pf = { "time", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(ABP_ResourceManager_C__pf2132744816, bpv__time__pf), METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__time__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__time__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__K2Node_Event_DeltaSeconds__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_b0l__Temp_int_Variable__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__hasWar__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonePercent__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__energy__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__solarPlant__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__coalPlant__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__resourseCount__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__ozonCount__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__growthRate__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__people__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::NewProp_bpv__time__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABP_ResourceManager_C__pf2132744816>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::ClassParams = {
		&ABP_ResourceManager_C__pf2132744816::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/BP_ResourceManager"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("BP_ResourceManager_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(ABP_ResourceManager_C__pf2132744816, TEXT("BP_ResourceManager_C"), 3786758468);
	template<> NATIVIZEDASSETS_API UClass* StaticClass<ABP_ResourceManager_C__pf2132744816>()
	{
		return ABP_ResourceManager_C__pf2132744816::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABP_ResourceManager_C__pf2132744816(Z_Construct_UClass_ABP_ResourceManager_C__pf2132744816, &ABP_ResourceManager_C__pf2132744816::StaticClass, TEXT("/Game/Blueprints/BP_ResourceManager"), TEXT("BP_ResourceManager_C"), true, TEXT("/Game/Blueprints/BP_ResourceManager"), TEXT("/Game/Blueprints/BP_ResourceManager.BP_ResourceManager_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABP_ResourceManager_C__pf2132744816);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
