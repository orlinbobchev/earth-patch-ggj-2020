// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NATIVIZEDASSETS_UI_GameOVer__pf3053510930_generated_h
#error "UI_GameOVer__pf3053510930.generated.h already included, missing '#pragma once' in UI_GameOVer__pf3053510930.h"
#endif
#define NATIVIZEDASSETS_UI_GameOVer__pf3053510930_generated_h

#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_SPARSE_DATA
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__GetText_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_0__pf(); \
		P_NATIVE_END; \
	}


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__GetText_0__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FText*)Z_Param__Result=P_THIS->bpf__GetText_0__pf(); \
		P_NATIVE_END; \
	}


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUI_GameOVer_C__pf3053510930(); \
	friend struct Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics; \
public: \
	DECLARE_CLASS(UUI_GameOVer_C__pf3053510930, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/UI/UI_GameOVer"), NO_API) \
	DECLARE_SERIALIZER(UUI_GameOVer_C__pf3053510930) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUUI_GameOVer_C__pf3053510930(); \
	friend struct Z_Construct_UClass_UUI_GameOVer_C__pf3053510930_Statics; \
public: \
	DECLARE_CLASS(UUI_GameOVer_C__pf3053510930, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Game/Blueprints/UI/UI_GameOVer"), NO_API) \
	DECLARE_SERIALIZER(UUI_GameOVer_C__pf3053510930) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUI_GameOVer_C__pf3053510930(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUI_GameOVer_C__pf3053510930) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUI_GameOVer_C__pf3053510930); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUI_GameOVer_C__pf3053510930); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUI_GameOVer_C__pf3053510930(UUI_GameOVer_C__pf3053510930&&); \
	NO_API UUI_GameOVer_C__pf3053510930(const UUI_GameOVer_C__pf3053510930&); \
public:


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUI_GameOVer_C__pf3053510930(UUI_GameOVer_C__pf3053510930&&); \
	NO_API UUI_GameOVer_C__pf3053510930(const UUI_GameOVer_C__pf3053510930&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUI_GameOVer_C__pf3053510930); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUI_GameOVer_C__pf3053510930); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUI_GameOVer_C__pf3053510930)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_PRIVATE_PROPERTY_OFFSET
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_8_PROLOG
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_RPC_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_INCLASS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_INCLASS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h_12_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NATIVIZEDASSETS_API UClass* StaticClass<class UUI_GameOVer_C__pf3053510930>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_UI_GameOVer__pf3053510930_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
