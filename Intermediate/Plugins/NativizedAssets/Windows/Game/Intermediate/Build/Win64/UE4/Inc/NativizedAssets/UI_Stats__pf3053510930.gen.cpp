// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NativizedAssets/Public/UI_Stats__pf3053510930.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUI_Stats__pf3053510930() {}
// Cross Module References
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_UUI_Stats_C__pf3053510930_NoRegister();
	NATIVIZEDASSETS_API UClass* Z_Construct_UClass_UUI_Stats_C__pf3053510930();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT();
	NATIVIZEDASSETS_API UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT();
	UMG_API UClass* Z_Construct_UClass_UImage_NoRegister();
	UMG_API UClass* Z_Construct_UClass_UTextBlock_NoRegister();
// End Cross Module References
	void UUI_Stats_C__pf3053510930::StaticRegisterNativesUUI_Stats_C__pf3053510930()
	{
		UClass* Class = UUI_Stats_C__pf3053510930::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCoal", &UUI_Stats_C__pf3053510930::execbpf__GetCoal__pf },
			{ "GetEnergy", &UUI_Stats_C__pf3053510930::execbpf__GetEnergy__pf },
			{ "GetResources", &UUI_Stats_C__pf3053510930::execbpf__GetResources__pf },
			{ "GetSolar", &UUI_Stats_C__pf3053510930::execbpf__GetSolar__pf },
			{ "GetText_0", &UUI_Stats_C__pf3053510930::execbpf__GetText_0__pf },
			{ "Get Ozone", &UUI_Stats_C__pf3053510930::execbpf__GetxOzone__pfT },
			{ "Get Population", &UUI_Stats_C__pf3053510930::execbpf__GetxPopulation__pfT },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics
	{
		struct UI_Stats_C__pf3053510930_eventbpf__GetCoal__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_Stats_C__pf3053510930_eventbpf__GetCoal__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "GetCoal" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Stats_C__pf3053510930, nullptr, "GetCoal", nullptr, nullptr, sizeof(UI_Stats_C__pf3053510930_eventbpf__GetCoal__pf_Parms), Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf()
	{
		UObject* Outer = Z_Construct_UClass_UUI_Stats_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetCoal") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics
	{
		struct UI_Stats_C__pf3053510930_eventbpf__GetEnergy__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_Stats_C__pf3053510930_eventbpf__GetEnergy__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "GetEnergy" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Stats_C__pf3053510930, nullptr, "GetEnergy", nullptr, nullptr, sizeof(UI_Stats_C__pf3053510930_eventbpf__GetEnergy__pf_Parms), Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf()
	{
		UObject* Outer = Z_Construct_UClass_UUI_Stats_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetEnergy") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics
	{
		struct UI_Stats_C__pf3053510930_eventbpf__GetResources__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_Stats_C__pf3053510930_eventbpf__GetResources__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "GetResources" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Stats_C__pf3053510930, nullptr, "GetResources", nullptr, nullptr, sizeof(UI_Stats_C__pf3053510930_eventbpf__GetResources__pf_Parms), Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf()
	{
		UObject* Outer = Z_Construct_UClass_UUI_Stats_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetResources") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics
	{
		struct UI_Stats_C__pf3053510930_eventbpf__GetSolar__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_Stats_C__pf3053510930_eventbpf__GetSolar__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "GetSolar" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Stats_C__pf3053510930, nullptr, "GetSolar", nullptr, nullptr, sizeof(UI_Stats_C__pf3053510930_eventbpf__GetSolar__pf_Parms), Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf()
	{
		UObject* Outer = Z_Construct_UClass_UUI_Stats_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetSolar") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics
	{
		struct UI_Stats_C__pf3053510930_eventbpf__GetText_0__pf_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_Stats_C__pf3053510930_eventbpf__GetText_0__pf_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "GetText_0" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Stats_C__pf3053510930, nullptr, "GetText_0", nullptr, nullptr, sizeof(UI_Stats_C__pf3053510930_eventbpf__GetText_0__pf_Parms), Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf()
	{
		UObject* Outer = Z_Construct_UClass_UUI_Stats_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("GetText_0") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics
	{
		struct UI_Stats_C__pf3053510930_eventbpf__GetxOzone__pfT_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_Stats_C__pf3053510930_eventbpf__GetxOzone__pfT_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Get Ozone" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Stats_C__pf3053510930, nullptr, "Get Ozone", nullptr, nullptr, sizeof(UI_Stats_C__pf3053510930_eventbpf__GetxOzone__pfT_Parms), Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT()
	{
		UObject* Outer = Z_Construct_UClass_UUI_Stats_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Get Ozone") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics
	{
		struct UI_Stats_C__pf3053510930_eventbpf__GetxPopulation__pfT_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UI_Stats_C__pf3053510930_eventbpf__GetxPopulation__pfT_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::Function_MetaDataParams[] = {
		{ "Category", "" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Get Population" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUI_Stats_C__pf3053510930, nullptr, "Get Population", nullptr, nullptr, sizeof(UI_Stats_C__pf3053510930_eventbpf__GetxPopulation__pfT_Parms), Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::PropPointers), RF_Public|RF_Transient, (EFunctionFlags)0x14020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT()
	{
		UObject* Outer = Z_Construct_UClass_UUI_Stats_C__pf3053510930();
		UFunction* ReturnFunction = static_cast<UFunction*>(StaticFindObjectFast( UFunction::StaticClass(), Outer, TEXT("Get Population") ));
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUI_Stats_C__pf3053510930_NoRegister()
	{
		return UUI_Stats_C__pf3053510930::StaticClass();
	}
	struct Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Time__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__Time__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Energy__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__Energy__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Solar__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__Solar__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Coal__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__Coal__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Resources__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__Resources__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Ozone__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_bpv__Ozone__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Population__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_bpv__Population__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Warning__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Warning__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__War__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__War__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_449__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_449__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_401__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_401__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_362__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_362__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_326__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_326__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_250__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_250__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_195__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_195__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__TextBlock_2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__TextBlock_2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_409__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_409__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_7__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_7__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_6__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_6__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_5__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_5__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_4__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_4__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_3__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_3__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image_1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image_1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Image__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Image__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Icon_5__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Icon_5__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Icon_4__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Icon_4__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Icon_3__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Icon_3__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Icon_2__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Icon_2__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Icon_1__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Icon_1__pf;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bpv__Icon__pf_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_bpv__Icon__pf;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetCoal__pf, "GetCoal" }, // 4121136627
		{ &Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetEnergy__pf, "GetEnergy" }, // 1015596714
		{ &Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetResources__pf, "GetResources" }, // 4121700727
		{ &Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetSolar__pf, "GetSolar" }, // 88504882
		{ &Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetText_0__pf, "GetText_0" }, // 3548843638
		{ &Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxOzone__pfT, "Get Ozone" }, // 429808618
		{ &Z_Construct_UFunction_UUI_Stats_C__pf3053510930_bpf__GetxPopulation__pfT, "Get Population" }, // 476414122
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "UI_Stats__pf3053510930.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "OverrideNativeName", "UI_Stats_C" },
		{ "ReplaceConverted", "/Game/Blueprints/UI/UI_Stats.UI_Stats_C" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Time__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Time" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Time" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Time__pf = { "Time", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Time__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Time__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Time__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Energy__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Energy" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Energy" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Energy__pf = { "Energy", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Energy__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Energy__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Energy__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Solar__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Solar" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Solar" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Solar__pf = { "Solar", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Solar__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Solar__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Solar__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Coal__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Coal" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Coal" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Coal__pf = { "Coal", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Coal__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Coal__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Coal__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Resources__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Resources" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Resources" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Resources__pf = { "Resources", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Resources__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Resources__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Resources__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Ozone__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Ozone" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Ozone" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Ozone__pf = { "Ozone", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Ozone__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Ozone__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Ozone__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Population__pf_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Population" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "MultiLine", "true" },
		{ "OverrideNativeName", "Population" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Population__pf = { "Population", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Population__pf), METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Population__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Population__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Warning__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Warning" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Warning" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Warning__pf = { "Warning", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Warning__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Warning__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Warning__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__War__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "War" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "War" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__War__pf = { "War", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__War__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__War__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__War__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_449__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_449" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_449" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_449__pf = { "TextBlock_449", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__TextBlock_449__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_449__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_449__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_401__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_401" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_401" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_401__pf = { "TextBlock_401", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__TextBlock_401__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_401__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_401__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_362__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_362" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_362" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_362__pf = { "TextBlock_362", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__TextBlock_362__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_362__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_362__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_326__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_326" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_326" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_326__pf = { "TextBlock_326", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__TextBlock_326__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_326__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_326__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_250__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_250" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_250" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_250__pf = { "TextBlock_250", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__TextBlock_250__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_250__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_250__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_195__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_195" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_195" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_195__pf = { "TextBlock_195", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__TextBlock_195__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_195__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_195__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_2__pf_MetaData[] = {
		{ "DisplayName", "TextBlock_2" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "TextBlock_2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_2__pf = { "TextBlock_2", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__TextBlock_2__pf), Z_Construct_UClass_UTextBlock_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_2__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_409__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_409" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_409" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_409__pf = { "Image_409", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_409__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_409__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_409__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_7__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_7" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_7" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_7__pf = { "Image_7", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_7__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_7__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_7__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_6__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_6" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_6" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_6__pf = { "Image_6", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_6__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_6__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_6__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_5__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_5" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_5" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_5__pf = { "Image_5", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_5__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_5__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_5__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_4__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_4" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_4" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_4__pf = { "Image_4", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_4__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_4__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_4__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_3__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_3" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_3" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_3__pf = { "Image_3", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_3__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_3__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_3__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_2__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_2" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_2__pf = { "Image_2", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_2__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_2__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_1__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image_1" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image_1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_1__pf = { "Image_1", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image_1__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_1__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Image" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Image" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image__pf = { "Image", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Image__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_5__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Icon_5" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Icon_5" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_5__pf = { "Icon_5", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Icon_5__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_5__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_5__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_4__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Icon_4" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Icon_4" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_4__pf = { "Icon_4", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Icon_4__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_4__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_4__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_3__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Icon_3" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Icon_3" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_3__pf = { "Icon_3", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Icon_3__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_3__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_3__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_2__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Icon_2" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Icon_2" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_2__pf = { "Icon_2", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Icon_2__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_2__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_2__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_1__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Icon_1" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Icon_1" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_1__pf = { "Icon_1", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Icon_1__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_1__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_1__pf_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon__pf_MetaData[] = {
		{ "Category", "UI_Stats" },
		{ "DisplayName", "Icon" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/UI_Stats__pf3053510930.h" },
		{ "OverrideNativeName", "Icon" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon__pf = { "Icon", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient, 1, STRUCT_OFFSET(UUI_Stats_C__pf3053510930, bpv__Icon__pf), Z_Construct_UClass_UImage_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon__pf_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon__pf_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Time__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Energy__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Solar__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Coal__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Resources__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Ozone__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Population__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Warning__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__War__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_449__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_401__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_362__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_326__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_250__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_195__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__TextBlock_2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_409__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_7__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_6__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_5__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_4__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_3__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image_1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Image__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_5__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_4__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_3__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_2__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon_1__pf,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::NewProp_bpv__Icon__pf,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUI_Stats_C__pf3053510930>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::ClassParams = {
		&UUI_Stats_C__pf3053510930::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::PropPointers),
		0,
		0x00A010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUI_Stats_C__pf3053510930()
	{
		UPackage* OuterPackage = FindOrConstructDynamicTypePackage(TEXT("/Game/Blueprints/UI/UI_Stats"));
		UClass* OuterClass = Cast<UClass>(StaticFindObjectFast(UClass::StaticClass(), OuterPackage, TEXT("UI_Stats_C")));
		if (!OuterClass || !(OuterClass->ClassFlags & CLASS_Constructed))
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUI_Stats_C__pf3053510930_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_DYNAMIC_CLASS(UUI_Stats_C__pf3053510930, TEXT("UI_Stats_C"), 995464933);
	template<> NATIVIZEDASSETS_API UClass* StaticClass<UUI_Stats_C__pf3053510930>()
	{
		return UUI_Stats_C__pf3053510930::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUI_Stats_C__pf3053510930(Z_Construct_UClass_UUI_Stats_C__pf3053510930, &UUI_Stats_C__pf3053510930::StaticClass, TEXT("/Game/Blueprints/UI/UI_Stats"), TEXT("UI_Stats_C"), true, TEXT("/Game/Blueprints/UI/UI_Stats"), TEXT("/Game/Blueprints/UI/UI_Stats.UI_Stats_C"), nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUI_Stats_C__pf3053510930);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
