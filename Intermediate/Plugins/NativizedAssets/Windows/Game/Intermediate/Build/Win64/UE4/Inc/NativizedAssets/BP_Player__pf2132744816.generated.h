// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FKey;
#ifdef NATIVIZEDASSETS_BP_Player__pf2132744816_generated_h
#error "BP_Player__pf2132744816.generated.h already included, missing '#pragma once' in BP_Player__pf2132744816.h"
#endif
#define NATIVIZEDASSETS_BP_Player__pf2132744816_generated_h

#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_SPARSE_DATA
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GamexOver__pfT) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GamexOver__pfT(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ExecuteUbergraph_BP_Player__pf_0) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_bpp__EntryPoint__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ExecuteUbergraph_BP_Player__pf_0(Z_Param_bpp__EntryPoint__pf); \
		P_NATIVE_END; \
	}


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_AddSolar_K2Node_InputActionEvent_3__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_AddCoal_K2Node_InputActionEvent_2__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_RemoveCoal_K2Node_InputActionEvent_1__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf) \
	{ \
		P_GET_STRUCT(FKey,Z_Param_bpp__Key__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpActEvt_ShowOzone_K2Node_InputActionEvent_0__pf(Z_Param_bpp__Key__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_CameraHorizontal_K2Node_InputAxisEvent_3__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_CameraVertical_K2Node_InputAxisEvent_4__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__AxisValue__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__InpAxisEvt_Zoom_K2Node_InputAxisEvent_5__pf(Z_Param_bpp__AxisValue__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveBeginPlay__pf) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveBeginPlay__pf(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ReceiveTick__pf) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_bpp__DeltaSeconds__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ReceiveTick__pf(Z_Param_bpp__DeltaSeconds__pf); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__GamexOver__pfT) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__GamexOver__pfT(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execbpf__ExecuteUbergraph_BP_Player__pf_0) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_bpp__EntryPoint__pf); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->bpf__ExecuteUbergraph_BP_Player__pf_0(Z_Param_bpp__EntryPoint__pf); \
		P_NATIVE_END; \
	}


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_EVENT_PARMS \
	struct BP_Player_C__pf2132744816_eventbpf__ReceiveTick__pf_Parms \
	{ \
		float bpp__DeltaSeconds__pf; \
	};


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_CALLBACK_WRAPPERS \
	void eventbpf__ReceiveBeginPlay__pf(); \
 \
	void eventbpf__ReceiveTick__pf(float bpp__DeltaSeconds__pf); \



#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABP_Player_C__pf2132744816(); \
	friend struct Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(ABP_Player_C__pf2132744816, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/BP_Player"), NO_API) \
	DECLARE_SERIALIZER(ABP_Player_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_INCLASS \
private: \
	static void StaticRegisterNativesABP_Player_C__pf2132744816(); \
	friend struct Z_Construct_UClass_ABP_Player_C__pf2132744816_Statics; \
public: \
	DECLARE_CLASS(ABP_Player_C__pf2132744816, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Game/Blueprints/BP_Player"), NO_API) \
	DECLARE_SERIALIZER(ABP_Player_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABP_Player_C__pf2132744816(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABP_Player_C__pf2132744816) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABP_Player_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABP_Player_C__pf2132744816); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABP_Player_C__pf2132744816(ABP_Player_C__pf2132744816&&); \
	NO_API ABP_Player_C__pf2132744816(const ABP_Player_C__pf2132744816&); \
public:


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABP_Player_C__pf2132744816(ABP_Player_C__pf2132744816&&); \
	NO_API ABP_Player_C__pf2132744816(const ABP_Player_C__pf2132744816&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABP_Player_C__pf2132744816); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABP_Player_C__pf2132744816); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABP_Player_C__pf2132744816)


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_PRIVATE_PROPERTY_OFFSET
#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_13_PROLOG \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_EVENT_PARMS


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_RPC_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_CALLBACK_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_INCLASS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_PRIVATE_PROPERTY_OFFSET \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_SPARSE_DATA \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_CALLBACK_WRAPPERS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_INCLASS_NO_PURE_DECLS \
	src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NATIVIZEDASSETS_API UClass* StaticClass<class ABP_Player_C__pf2132744816>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID src_Intermediate_Plugins_NativizedAssets_Windows_Game_Source_NativizedAssets_Public_BP_Player__pf2132744816_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
